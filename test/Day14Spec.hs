module Day14Spec (
  spec,
) where

import Data.Text (Text)
import Day14
import Test.Hspec
import Text.InterpolatedString.QM (qnb)
import qualified Data.Set as Set

sample :: Text
sample =
  [qnb|mask = XXXXXXXXXXXXXXXXXXXXXXXXXXXXX1XXXX0X
       mem[8] = 11
       mem[7] = 101
       mem[8] = 0|]

sample2 :: Text
sample2 =
  [qnb|mask = 000000000000000000000000000000X1001X
       mem[42] = 100
       mask = 00000000000000000000000000000000X0XX
       mem[26] = 1|]

spec :: Spec
spec = do
  describe "day14" $ do
    it "should parse input" $
      parseInput sample
        `shouldBe` Right
          Program
            { pInstructions =
                [ ISetMask (64, 2, Set.fromList [2 ^ x | x <- [0 .. 35], x /= 1, x /= 6])
                , IAssign 8 11
                , IAssign 7 101
                , IAssign 8 0
                ]
            }

    describe "part 1" $ do
      it "should set values correctly" $
        (sumValues . (`runProgram` initialState) <$> parseInput sample) `shouldBe` Right 165

    describe "part 2" $ do
      it "should set values correctly" $
        (sumValues . (`runProgramV2` initialState) <$> parseInput sample2) `shouldBe` Right 208
