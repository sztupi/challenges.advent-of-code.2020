module Day12Spec (
  spec,
) where

import Data.Text (Text)
import Day12
import Test.Hspec
import Text.InterpolatedString.QM (qnb)

sample :: Text
sample =
  [qnb|F10
       N3
       F7
       R90
       F11|]

spec :: Spec
spec = do
  describe "day12" $ do
    describe "parsing" $ do
      it "sample" $ do
        parseInput sample `shouldBe` Right [IForward 10, INorth 3, IForward 7, IRight 90, IForward 11]
    describe "part 1" $ do
      it "sample" $ do
        fPos . (`runInstructions` ferryStart) <$> parseInput sample `shouldBe` Right (17, 8)

    describe "part 2" $ do
      it "sample" $ do
        wfPos . (`runWaypoints` wferryStart) <$> parseInput sample `shouldBe` Right (214, 72)
      it "sample step positions" $ do
        fmap wfPos . scanl runWInstruction wferryStart <$> parseInput sample `shouldBe` Right [(0,0), (100, -10), (100, -10), (170, -38), (170, -38), (214, 72)]
      it "sample step waypoints" $ do
        fmap wfWaypoint . scanl runWInstruction wferryStart <$> parseInput sample `shouldBe` Right [(10,-1), (10, -1), (10, -4), (10, -4), (4, 10), (4, 10)]