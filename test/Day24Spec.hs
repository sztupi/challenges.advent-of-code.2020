module Day24Spec (
  spec,
) where

import Data.Text (Text)
import Day24
import Test.Hspec
import Text.InterpolatedString.QM (qnb)

sample :: Text
sample =
  [qnb|sesenwnenenewseeswwswswwnenewsewsw
       neeenesenwnwwswnenewnwwsewnenwseswesw
       seswneswswsenwwnwse
       nwnwneseeswswnenewneswwnewseswneseene
       swweswneswnenwsewnwneneseenw
       eesenwseswswnenwswnwnwsewwnwsene
       sewnenenenesenwsewnenwwwse
       wenwwweseeeweswwwnwwe
       wsweesenenewnwwnwsenewsenwwsesesenwne
       neeswseenwwswnwswswnw
       nenwswwsewswnenenewsenwsenwnesesenew
       enewnwewneswsewnwswenweswnenwsenwsw
       sweneswneswneneenwnewenewwneswswnese
       swwesenesewenwneswnwwneseswwne
       enesenwswwswneneswsenwnewswseenwsese
       wnwnesenesenenwwnenwsewesewsesesew
       nenewswnwewswnenesenwnesewesw
       eneswnwswnwsenenwnwnwwseeswneewsenese
       neswnwewnwnwseenwseesewsenwsweewe
       wseweeenwnesenwwwswnew|]

spec :: Spec
spec = do
  describe "day24" $ do
    describe "part 1" $ do
      it "sample" $
        let (Right input) = parseInput sample
         in findBlackTileCount input `shouldBe` 10

    describe "part 2" $ do
      describe "samples" $ do
        it "step 1" $
          let (Right input) = parseInput sample
           in runSteps 1 input `shouldBe` 15

        it "step 2" $
          let (Right input) = parseInput sample
           in runSteps 2 input `shouldBe` 12

        it "step 100" $
          let (Right input) = parseInput sample
           in runSteps 100 input `shouldBe` 2208