module Day22Spec (
  spec,
) where

import Data.Text (Text)
import Day22
import Test.Hspec
import Text.InterpolatedString.QM (qnb)

sample :: Text
sample =
  [qnb|Player 1:
       9
       2
       6
       3
       1

       Player 2:
       5
       8
       4
       7
       10|]

spec :: Spec
spec = do
  describe "day22" $ do
    describe "parsing" $ do
      it "sample" $
        parseInput sample
          `shouldBe` Right ([9, 2, 6, 3, 1], [5, 8, 4, 7, 10])

    let game = ([9, 2, 6, 3, 1], [5, 8, 4, 7, 10])

    describe "part 1" $ do
      it "sample" $
        runGame game `shouldBe` ([], [3, 2, 10, 6, 8, 5, 9, 4, 7, 1])

      it "score" $
        (scoreGame . runGame) game `shouldBe` (0, 306)

    describe "part 2" $ do
      it "infinite prevention" $ do
        runRecGame ([43, 19], [2, 29, 14]) `shouldBe` (P1, ([43, 19], [2, 29, 14]))

      it "rec game" $ do
        runRecGame game `shouldBe` (P2, ([], [7, 5, 6, 2, 4, 1, 10, 8, 9, 3]))