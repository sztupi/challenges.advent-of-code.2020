module Day19Spec (
  spec,
) where

import Data.Bifunctor
import qualified Data.Map.Strict as Map
import Data.Text (Text)
import Day19
import Test.Hspec
import Text.InterpolatedString.QM (qnb)

sample :: Text
sample =
  [qnb|0: 4 1 5
       1: 2 3 | 3 2
       2: 4 4 | 5 5
       3: 4 5 | 5 4
       4: "a"
       5: "b"

       ababbb
       bababa
       abbbab
       aaabbb
       aaaabbb|]

sample2 :: Text
sample2 =
  [qnb|42: 9 14 | 10 1
       9: 14 27 | 1 26
       10: 23 14 | 28 1
       1: "a"
       11: 42 31
       5: 1 14 | 15 1
       19: 14 1 | 14 14
       12: 24 14 | 19 1
       16: 15 1 | 14 14
       31: 14 17 | 1 13
       6: 14 14 | 1 14
       2: 1 24 | 14 4
       0: 8 11
       13: 14 3 | 1 12
       15: 1 | 14
       17: 14 2 | 1 7
       23: 25 1 | 22 14
       28: 16 1
       4: 1 1
       20: 14 14 | 1 15
       3: 5 14 | 16 1
       27: 1 6 | 14 18
       14: "b"
       21: 14 1 | 1 14
       25: 1 1 | 1 14
       22: 14 14
       8: 42
       26: 14 22 | 1 20
       18: 15 15
       7: 14 5 | 1 21
       24: 14 1

       abbbbbabbbaaaababbaabbbbabababbbabbbbbbabaaaa
       bbabbbbaabaabba
       babbbbaabbbbbabbbbbbaabaaabaaa
       aaabbbbbbaaaabaababaabababbabaaabbababababaaa
       bbbbbbbaaaabbbbaaabbabaaa
       bbbababbbbaaaaaaaabbababaaababaabab
       ababaaaaaabaaab
       ababaaaaabbbaba
       baabbaaaabbaaaababbaababb
       abbbbabbbbaaaababbbbbbaaaababb
       aaaaabbaabaaaaababaa
       aaaabbaaaabbaaa
       aaaabbaabbaaaaaaabbbabbbaaabbaabaaa
       babaaabbbaaabaababbaabababaaab
       aabbbbbaabbbaaaaaabbbbbababaaaaabbaaabba|]

spec :: Spec
spec = do
  describe "day19" $ do
    describe "parsing" $ do
      it "should parse sample" $
        parseInput sample
          `shouldBe` ( Right $
                        Map.fromList
                          [ (0, RSeq [RSub 4, RSub 1, RSub 5])
                          , (1, REither [RSeq [RSub 2, RSub 3], RSeq [RSub 3, RSub 2]])
                          , (2, REither [RSeq [RSub 4, RSub 4], RSeq [RSub 5, RSub 5]])
                          , (3, REither [RSeq [RSub 4, RSub 5], RSeq [RSub 5, RSub 4]])
                          , (4, RChar 'a')
                          , (5, RChar 'b')
                          ]
                     ,
                       [ "ababbb"
                       , "bababa"
                       , "abbbab"
                       , "aaabbb"
                       , "aaaabbb"
                       ]
                     )

    describe "part 1" $ do
      it "flatten" $
        (fmap flattenRules . fst . parseInput $ sample)
          `shouldBe` Right
            ( Right $
                RSeq
                  [ RChar 'a'
                  , REither
                      [ RSeq
                          [ REither
                              [ RSeq
                                  [ RChar 'a'
                                  , RChar 'a'
                                  ]
                              , RSeq
                                  [ RChar 'b'
                                  , RChar 'b'
                                  ]
                              ]
                          , REither
                              [ RSeq
                                  [ RChar 'a'
                                  , RChar 'b'
                                  ]
                              , RSeq
                                  [ RChar 'b'
                                  , RChar 'a'
                                  ]
                              ]
                          ]
                      , RSeq
                          [ REither
                              [ RSeq
                                  [ RChar 'a'
                                  , RChar 'b'
                                  ]
                              , RSeq
                                  [ RChar 'b'
                                  , RChar 'a'
                                  ]
                              ]
                          , REither
                              [ RSeq
                                  [ RChar 'a'
                                  , RChar 'a'
                                  ]
                              , RSeq
                                  [ RChar 'b'
                                  , RChar 'b'
                                  ]
                              ]
                          ]
                      ]
                  , RChar 'b'
                  ]
            )

      it "sample" $
        (fmap length . uncurry findMatchingMessages . parseInput) sample
          `shouldBe` Right 2

    describe "part2" $ do
      it "sample2 unfixed" $
        (fmap length . uncurry findMatchingMessages . parseInput) sample2
          `shouldBe` Right 3

      it "sample2 fixed all" $
        (uncurry findMatchingMessages . first (fmap rulesUpdate) . parseInput) sample2
          `shouldBe` Right
            [ "bbabbbbaabaabba"
            , "babbbbaabbbbbabbbbbbaabaaabaaa"
            , "aaabbbbbbaaaabaababaabababbabaaabbababababaaa"
            , "bbbbbbbaaaabbbbaaabbabaaa"
            , "bbbababbbbaaaaaaaabbababaaababaabab"
            , "ababaaaaaabaaab"
            , "ababaaaaabbbaba"
            , "baabbaaaabbaaaababbaababb"
            , "abbbbabbbbaaaababbbbbbaaaababb"
            , "aaaaabbaabaaaaababaa"
            , "aaaabbaabbaaaaaaabbbabbbaaabbaabaaa"
            , "aabbbbbaabbbaaaaaabbbbbababaaaaabbaaabba"
            ]

      it "sample2 fixed" $
        (fmap length . uncurry findMatchingMessages . first (fmap rulesUpdate) . parseInput) sample2
          `shouldBe` Right 12
