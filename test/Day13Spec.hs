module Day13Spec (
  spec,
) where

import Data.Text (Text)
import Day13
import Test.Hspec
import Text.InterpolatedString.QM (qnb)

sample :: Text
sample =
  [qnb|939
       7,13,x,x,59,x,31,19|]

spec :: Spec
spec = do
  describe "day13" $ do
    describe "part 1" $ do
      it "should find bus and time" $
        (findDepartBusAndDelay <$> parseInput sample) `shouldBe` Right (59, 5)

    describe "part 2" $ do
      it "sample 1" $
        findStart [Just 7, Just 13, Nothing, Nothing, Just 59, Nothing, Just 31, Just 19] `shouldBe` 1068781
      it "sample 2" $
        findStart [Just 17, Nothing, Just 13, Just 19] `shouldBe` 3417
      it "sample 3" $
        findStart [Just 67, Just 7, Just 59, Just 61] `shouldBe` 754018
      it "sample 4" $
        findStart [Just 67, Nothing, Just 7, Just 59, Just 61] `shouldBe` 779210
      it "sample 5" $
        findStart [Just 67, Just 7, Nothing, Just 59, Just 61] `shouldBe` 1261476
      it "sample 6" $
        findStart [Just 1789, Just 37, Just 47, Just 1889] `shouldBe` 1202161486