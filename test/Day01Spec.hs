module Day01Spec
  ( spec
  )
where

import           Test.Hspec
import           Day01

spec :: Spec
spec =
  describe "day01" $ do
    describe "part 1" $ do
      describe "tests" $ do
        it "should find exact match" $
          findAddUpMult 1 10 [2, 20, 10, 3] `shouldBe` Just 10
        it "should find match in minimal elements" $
          findAddUpMult 2 5 [2, 3] `shouldBe` Just 6
        it "should find match among extra elements 1" $
          findAddUpMult 2 5 [1, 2, 3] `shouldBe` Just 6

      describe "examples" $
        it "example01" $
          findAddUpMult 2 2020 [1721, 979, 366, 299, 675, 1456] `shouldBe` Just 514579

    describe "part 2" $
      describe "examples" $
        it "example01" $
          findAddUpMult 3 2020 [1721, 979, 366, 299, 675, 1456] `shouldBe` Just 241861950
