module Day07Spec
  ( spec,
  )
where

import qualified Data.Map.Strict as Map
import qualified Data.Set as Set
import Data.Text (Text)
import Day07
import Test.Hspec
import Text.InterpolatedString.QM (qnb)

sample :: Text
sample =
  [qnb|light red bags contain 1 bright white bag, 2 muted yellow bags.
       dark orange bags contain 3 bright white bags, 4 muted yellow bags.
       bright white bags contain 1 shiny gold bag.
       muted yellow bags contain 2 shiny gold bags, 9 faded blue bags.
       shiny gold bags contain 1 dark olive bag, 2 vibrant plum bags.
       dark olive bags contain 3 faded blue bags, 4 dotted black bags.
       vibrant plum bags contain 5 faded blue bags, 6 dotted black bags.
       faded blue bags contain no other bags.
       dotted black bags contain no other bags.|]

spec :: Spec
spec = do
  describe "day07" $ do
    describe "part 1" $ do
      it "sample" $
        countPossibleContainerColors "shiny gold" sample `shouldBe` Right 4

      describe "parse rules" $ do
        it "parse sample rules correctly" $
          parseRules sample
            `shouldBe` Right
              ( Map.fromList
                  [ ("light red", Map.fromList [("bright white", 1), ("muted yellow", 2)]),
                    ("dark orange", Map.fromList [("bright white", 3), ("muted yellow", 4)]),
                    ("bright white", Map.fromList [("shiny gold", 1)]),
                    ("muted yellow", Map.fromList [("shiny gold", 2), ("faded blue", 9)]),
                    ("shiny gold", Map.fromList [("dark olive", 1), ("vibrant plum", 2)]),
                    ("dark olive", Map.fromList [("faded blue", 3), ("dotted black", 4)]),
                    ("vibrant plum", Map.fromList [("faded blue", 5), ("dotted black", 6)]),
                    ("faded blue", Map.empty),
                    ("dotted black", Map.empty)
                  ]
              )

      describe "findAllParents" $ do
        it "directParents in sample rules correctly" $
          let (Right rules) = parseRules sample
              dparents = directParents rules
           in dparents
                `shouldBe` Map.fromList
                  [ ("bright white", Set.fromList ["light red", "dark orange"]),
                    ("muted yellow", Set.fromList ["light red", "dark orange"]),
                    ("shiny gold", Set.fromList ["bright white", "muted yellow"]),
                    ("dark olive", Set.fromList ["shiny gold"]),
                    ("vibrant plum", Set.fromList ["shiny gold"]),
                    ("faded blue", Set.fromList ["muted yellow", "dark olive", "vibrant plum"]),
                    ("dotted black", Set.fromList ["dark olive", "vibrant plum"])
                  ]

        it "findAllParents in sample rules correctly" $
          let (Right rules) = parseRules sample
              dparents = directParents rules
              parents = findAllParents dparents "shiny gold"
           in parents `shouldBe` Set.fromList ["bright white", "muted yellow", "dark orange", "light red"]

    describe "part 2" $ do
      describe "count bags" $ do
        describe "samples" $ do
          it "sample 1" $
            countContainedBags "shiny gold" sample `shouldBe` Right 32