module Day06Spec
  ( spec,
  )
where

import Data.Text (Text)
import qualified Data.Text as Text
import Day06
import Test.Hspec
import Text.InterpolatedString.QM (qnb)

sample :: Text
sample =
  [qnb|
    abc
    
    a
    b
    c
    
    ab
    ac
    
    a
    a
    a
    a
    
    b|]

spec :: Spec
spec = do
  describe "day06" $ do
    describe "part 1" $ do
      it "sample" $
        countAnyoneQuestions (Text.lines sample) `shouldBe` [3, 3, 3, 1, 1]

    describe "part 2" $ do
      it "sample" $
        countEveryoneQuestions (Text.lines sample) `shouldBe` [3, 0, 1, 1, 1]