module Day15Spec (
  spec,
) where

import Control.Monad (forM_)
import qualified Data.Text as Text
import Day15
import Test.Hspec

spec :: Spec
spec = do
  describe "day15" $ do
    describe "part 1" $ do
      describe "samples" $
        forM_
          [ ("0,3,6", 436)
          , ("1,3,2", 1)
          , ("2,1,3", 10)
          ]
          $ \(input, exp) -> do
            it (Text.unpack input) $
              (findStep 2020 <$> parseInput input) `shouldBe` Right exp