module Day17Spec (
  spec,
) where

import qualified Data.Set as Set
import Data.Text (Text)
import Day17
import Test.Hspec
import Text.InterpolatedString.QM (qnb)

sample :: Text
sample =
  [qnb|.#.
       ..#
       ###|]

spec :: Spec
spec = do
  describe "day17" $ do
    describe "parsing" $ do
      it "should parse sample" $
        parseInput @Coord3 sample `shouldBe` Set.fromList [(1, 0, 0), (2, 1, 0), (0, 2, 0), (1, 2, 0), (2, 2, 0)]

    describe "part 1" $ do
      it "sample" $
        (Set.size . steps 6 . parseInput @Coord3) sample `shouldBe` 112

    describe "part 2" $ do
      it "sample" $
        (Set.size . steps 6 . parseInput @Coord4) sample `shouldBe` 848