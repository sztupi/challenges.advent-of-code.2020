module Day16Spec (
  spec,
) where

import Data.Text (Text)
import Day16
import Test.Hspec
import Text.InterpolatedString.QM (qnb)
import qualified Data.Set as Set

sample :: Text
sample =
  [qnb|class: 1-3 or 5-7
       row: 6-11 or 33-44
       seat: 13-40 or 45-50

       your ticket:
       7,1,14

       nearby tickets:
       7,3,47
       40,4,50
       55,2,20
       38,6,12|]

sample2 :: Text
sample2 =
  [qnb|class: 0-1 or 4-19
       row: 0-5 or 8-19
       seat: 0-13 or 16-19

       your ticket:
       11,12,13

       nearby tickets:
       3,9,18
       15,1,5
       5,14,9|]

spec :: Spec
spec = do
  describe "day16" $ do
    describe "parsing" $ do
      it "should parse sample" $
        parseInput sample
          `shouldBe` Right
            Input
              { iReqs =
                  [ Req "class" [(1, 3), (5, 7)]
                  , Req "row" [(6, 11), (33, 44)]
                  , Req "seat" [(13, 40), (45, 50)]
                  ]
              , iMyTicket = [7, 1, 14]
              , iOtherTickets =
                  [ [7, 3, 47]
                  , [40, 4, 50]
                  , [55, 2, 20]
                  , [38, 6, 12]
                  ]
              }
    describe "part 1" $ do
      it "should find scanningErrorRate" $
        (scanningErrorRate <$> parseInput sample) `shouldBe` Right 71

    describe "part 2" $ do
      it "should drop invalid tickets" $
        (iOtherTickets . dropInvalidTickets <$> parseInput sample)
          `shouldBe` Right [[7, 3, 47]]

      it "should find labels correctly" $
        (inferLabels <$> parseInput sample2)
          `shouldBe` Right (Set.singleton <$> ["row", "class", "seat"])
