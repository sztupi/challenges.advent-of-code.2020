module Day11Spec (
  spec,
) where

import Data.Text (Text)
import Day11
import Test.Hspec
import Text.InterpolatedString.QM (qnb)

sample :: Text
sample =
  [qnb|L.LL.LL.LL
       LLLLLLL.LL
       L.L.L..L..
       LLLL.LL.LL
       L.LL.LL.LL
       L.LLLLL.LL
       ..L.L.....
       LLLLLLLLLL
       L.LLLLLL.L
       L.LLLLL.LL|]

step1exp :: Text
step1exp =
  [qnb|#.##.##.##
       #######.##
       #.#.#..#..
       ####.##.##
       #.##.##.##
       #.#####.##
       ..#.#.....
       ##########
       #.######.#
       #.#####.##|]

step3exp :: Text
step3exp =
  [qnb|#.##.L#.##
       #L###LL.L#
       L.#.#..#..
       #L##.##.L#
       #.##.LL.LL
       #.###L#.##
       ..#.#.....
       #L######L#
       #.LL###L.L
       #.#L###.##|]

step3expVisible :: Text
step3expVisible =
  [qnb|#.L#.##.L#
       #L#####.LL
       L.#.#..#..
       ##L#.##.##
       #.##.#L.##
       #.#####.#L
       ..#.#.....
       LLL####LL#
       #.L#####.L
       #.L####.L#|]

spec :: Spec
spec = do
  describe "day11" $ do
    describe "part 1" $ do
      describe "sample steps" $ do
        it "step1" $
          let init = parseInput sample
              step1 = (!! 1) . runSteps simpleStep $ init
           in step1 `shouldBe` parseInput step1exp

        it "step3" $
          let init = parseInput sample
              step3 = (!! 3) . runSteps simpleStep $ init
           in step3 `shouldBe` parseInput step3exp

      it "find stable" $
        let init = parseInput sample
            stable = findStable simpleStep init
         in countOccupied stable `shouldBe` 37

    describe "part 2" $ do
      describe "sample steps" $ do
        it "step1b" $
          let init = parseInput sample
              step1 = (!! 1) . runSteps visibleStep $ init
           in step1 `shouldBe` parseInput step1exp

        it "step3b" $
          let init = parseInput sample
              step3 = (!! 3) . runSteps visibleStep $ init
           in step3 `shouldBe` parseInput step3expVisible

      it "find stable" $
        let init = parseInput sample
            stable = findStable visibleStep init
         in countOccupied stable `shouldBe` 26