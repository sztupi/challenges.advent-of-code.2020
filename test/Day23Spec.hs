module Day23Spec (
  spec,
) where

import Day23
import Test.Hspec

sample :: [Int]
sample = parseInput "389125467"

spec :: Spec
spec = do
  describe "day23" $ do
    let step0 = sample
        step1 = [2, 8, 9, 1, 5, 4, 6, 7, 3]
        step2 = [5, 4, 6, 7, 8, 9, 1, 3, 2]
        step3 = [8, 9, 1, 3, 4, 6, 7, 2, 5]

    describe "part 1" $ do
      describe "sample steps" $ do
        let s = gameToList . (\g -> gameStep (bounds g) g) . gameFromList
        it "step 1" $
          s step0 `shouldBe` step1
        it "step 2" $
          s step1 `shouldBe` step2
        it "step 3" $
          s step2 `shouldBe` step3

      it "sample" $
        runGame 100 sample `shouldBe` [1, 6, 7, 3, 8, 4, 5, 2, 9]

    describe "part 2" $ do
      describe "sample steps" $ do
        it "step 0" $
          snd (runGame2 9 0 sample) `shouldBe` [1, 2, 5, 4, 6, 7, 3, 8, 9]
        it "step 1" $
          snd (runGame2 9 1 sample) `shouldBe` [1, 5, 4, 6, 7, 3, 2, 8, 9]
        it "step 2" $
          snd (runGame2 9 2 sample) `shouldBe` [1, 3, 2, 5, 4, 6, 7, 8, 9]
        it "step 3" $
          snd (runGame2 9 3 sample) `shouldBe` [1, 3, 4, 6, 7, 2, 5, 8, 9]

      it "short sample" $
        runGame2 9 100 sample `shouldBe` ((6, 7), [1, 6, 7, 3, 8, 4, 5, 2, 9])

      it "full sample" $
        play2 sample `shouldBe` (934001, 159792)