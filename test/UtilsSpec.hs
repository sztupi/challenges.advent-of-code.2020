module UtilsSpec where

import Data.Char (isSpace)
import qualified Data.List as List
import Test.Hspec
import Test.QuickCheck
import Utils (groupings)

spec :: Spec
spec = do
  describe "utils" $ do
    groupingsSpec

groupingsSpec :: Spec
groupingsSpec = do
  describe "groupings" $ do
    it "parses back groups of texts separated by empty lines" $ do
      let lineG = do
            n <- chooseInt (1, 20)
            vectorOf n arbitraryPrintableChar `suchThat` (not . all isSpace)

          groupG = do
            n <- chooseInt (1, 20)
            vectorOf n lineG

          groupsG = do
            n <- chooseInt (1, 20)
            vectorOf n groupG

      forAllShrink groupsG (shrinkList (: [])) $ \gs -> do
        groupings (List.intercalate [""] gs) === gs
