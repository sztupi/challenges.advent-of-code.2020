module Day09Spec (
  spec,
) where

import Day09
import Test.Hspec

sample :: [Integer]
sample = [35, 20, 15, 25, 47, 40, 62, 55, 65, 95, 102, 117, 150, 182, 127, 219, 299, 277, 309, 576]

spec :: Spec
spec = do
  describe "day09" $ do
    describe "part 1" $ do
      it "sample" $
        findFirstInvalid 5 sample `shouldBe` Just 127

    describe "part 2" $ do
      it "sample" $
        findSetAddingUpToInvalid 5 sample `shouldBe` Just (15 + 47)
