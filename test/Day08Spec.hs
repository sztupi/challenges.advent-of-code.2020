module Day08Spec (
  spec,
) where

import Data.Text (Text)
import Day08
import Test.Hspec
import Text.InterpolatedString.QM (qnb)

sample :: Text
sample =
  [qnb|nop +0
       acc +1
       jmp +4
       acc +3
       jmp -3
       acc -99
       acc +1
       jmp -4
       acc +6|]

spec :: Spec
spec = do
  describe "day08" $ do
    describe "part 1" $ do
      it "sample" $
        (findRepeatInstructionAcc =<< parseProg sample) `shouldBe` Right 5

    describe "part 2" $ do
      it "sample" $
        (tryFixInstruction =<< parseProg sample) `shouldBe` Right 8
