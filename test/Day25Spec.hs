module Day25Spec (
  spec,
) where

import Data.Text (Text)
import Day25
import Test.Hspec
import Text.InterpolatedString.QM (qnb)

sample :: Text
sample =
  [qnb|5764801
       17807724|]

spec :: Spec
spec = do
  describe "day24" $ do
    describe "part 1" $ do
      it "sample" $
        findHandshake (parseInput sample) `shouldBe` 14897079