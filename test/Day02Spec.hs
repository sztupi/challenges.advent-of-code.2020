module Day02Spec
  ( spec,
  )
where

import qualified Data.Text as Text
import Day02
import Test.Hspec
import Test.QuickCheck
import Text.InterpolatedString.QM (qnb)

lower, upper, alpha, num, alphaNum :: [Char]
lower = ['a' .. 'z']
upper = ['A' .. 'Z']
alpha = lower <> upper
num = ['0' .. '9']
alphaNum = alpha <> num

instance Arbitrary PwPolicy where
  arbitrary = PwPolicy <$> elements alphaNum <*> range
    where
      range = do
        cmax <- arbitrary `suchThat` (>= 1)
        cmin <- elements [1 .. cmax]
        return (cmin, cmax)

instance Arbitrary PwLine where
  arbitrary = PwLine <$> arbitrary <*> (Text.pack <$> listOf1 (elements alphaNum))

spec :: Spec
spec =
  describe "day02" $ do
    parserSpecs
    verifySpecs
    verifyTobSpecs
    part1Specs

part1Specs :: Spec
part1Specs = do
  it "should ive correct result for examlpe" $
    let input =
          [qnb|1-3 a: abcde
               1-3 b: cdefg
               2-9 c: ccccccccc|]
     in countValidLines input `shouldBe` Right 2

verifySpecs :: Spec
verifySpecs = do
  let verifyText = fmap verify . parseLine
  describe "verify policy" $ do
    describe "should correctly verify examples" $ do
      it "ex1" $ verifyText "1-3 a: abcde" `shouldBe` Right True
      it "ex2" $ verifyText "1-3 b: cdefg" `shouldBe` Right False
      it "ex3" $ verifyText "2-9 c: ccccccccc" `shouldBe` Right True

verifyTobSpecs :: Spec
verifyTobSpecs = do
  let verifyTobText = fmap verifyTob . parseLine
  describe "verify policy" $ do
    describe "should correctly verify examples" $ do
      it "ex1" $ verifyTobText "1-3 a: abcde" `shouldBe` Right True
      it "ex2" $ verifyTobText "1-3 b: cdefg" `shouldBe` Right False
      it "ex3" $ verifyTobText "2-9 c: ccccccccc" `shouldBe` Right False

parserSpecs :: Spec
parserSpecs =
  describe "parser" $ do
    it "should parse example" $
      parseLine "1-3 b: cdefg"
        `shouldBe` Right PwLine {plPolicy = PwPolicy {ppChar = 'b', ppCount = (1, 3)}, plPassword = "cdefg"}
    it "should parse back all lines" $
      property $ \p -> (parseLine . lineToText) p === Right p
