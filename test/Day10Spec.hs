module Day10Spec (
  spec,
) where

import Day10
import Test.Hspec

sample1 :: [Int]
sample1 = [16, 10, 15, 5, 1, 11, 7, 19, 6, 12, 4]

sample2 :: [Int]
sample2 = [28, 33, 18, 42, 31, 14, 46, 20, 48, 47, 24, 23, 49, 45, 19, 38, 39, 11, 1, 32, 25, 35, 8, 17, 7, 9, 4, 2, 34, 10, 3]

spec :: Spec
spec = do
  describe "day10" $ do
    describe "part 1" $ do
      it "sample1" $
        multChainDiffs sample1 `shouldBe` 35

      it "sample2" $
        multChainDiffs sample2 `shouldBe` 220

    describe "part 2" $ do
      it "sample1" $
        numberOfValidChains sample1 `shouldBe` 8

      it "sample2" $
        numberOfValidChains sample2 `shouldBe` 19208