module Day05Spec
  ( spec,
  )
where

import Control.Monad (forM_)
import Day05
import Test.Hspec

spec :: Spec
spec = do
  describe "day05" $ do
    describe "boarding card examples" $ do
      describe "seat ids" $ do
        forM_
          [ ("FBFBBFFRLR", 357),
            ("BFFFBBFRRR", 567),
            ("FFFBBBFRRR", 119),
            ("BBFFBBFRLL", 820)
          ]
          $ \(card, expected) ->
            it (show card) $
              seatId card `shouldBe` expected