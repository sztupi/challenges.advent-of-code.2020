module Day21Spec (
  spec,
) where

import qualified Data.Set as Set
import Data.Text (Text)
import Day21
import Test.Hspec
import Text.InterpolatedString.QM (qnb)

sample :: Text
sample =
  [qnb|mxmxvkd kfcds sqjhc nhms (contains dairy, fish)
       trh fvjkl sbzzf mxmxvkd (contains dairy)
       sqjhc fvjkl (contains soy)
       sqjhc mxmxvkd sbzzf (contains fish)|]

spec :: Spec
spec = do
  describe "day21" $ do
    describe "parsing" $ do
      it "sample" $
        parseInput sample
          `shouldBe` Right
            [
              ( Set.fromList $ Ingredient <$> ["mxmxvkd", "kfcds", "sqjhc", "nhms"]
              , Set.fromList $ Allergen <$> ["dairy", "fish"]
              )
            ,
              ( Set.fromList $ Ingredient <$> ["trh", "fvjkl", "sbzzf", "mxmxvkd"]
              , Set.fromList $ Allergen <$> ["dairy"]
              )
            ,
              ( Set.fromList $ Ingredient <$> ["sqjhc", "fvjkl"]
              , Set.fromList $ Allergen <$> ["soy"]
              )
            ,
              ( Set.fromList $ Ingredient <$> ["sqjhc", "mxmxvkd", "sbzzf"]
              , Set.fromList $ Allergen <$> ["fish"]
              )
            ]

    describe "part 1" $ do
      let (Right input) = parseInput sample
      it "find non-allergenes" $
        findNonAllergens input `shouldBe` Set.fromList (Ingredient <$> ["kfcds", "nhms", "sbzzf", "trh"])

      it "find non-allergene count" $
        findNonAllergenOccurences input `shouldBe` 5

    describe "part 2" $ do
      let (Right input) = parseInput sample
      it "infer allergenes" $
        inferAllergens input `shouldBe` Ingredient <$> ["mxmxvkd", "sqjhc", "fvjkl"]