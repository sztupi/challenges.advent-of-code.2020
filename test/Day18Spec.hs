module Day18Spec (
  spec,
) where

import Control.Monad (forM_)
import qualified Data.Text as Text
import Day18
import Test.Hspec

spec :: Spec
spec = do
  describe "day18" $ do
    describe "parsing" $ do
      it "simple expressions" $
        parse1 "1 + 2 * 3" `shouldBe` Right (EOp OMult (EOp OAdd (ENumLit 1) (ENumLit 2)) (ENumLit 3))

      it "paren expressions" $
        parse1 "1 + (2 * 3) + 4"
          `shouldBe` Right
            (EOp OAdd (EOp OAdd (ENumLit 1) (EOp OMult (ENumLit 2) (ENumLit 3))) (ENumLit 4))

    describe "part 1" $ do
      describe "samples" $
        forM_
          [ ("2 * 3 + (4 * 5)", 26)
          , ("5 + (8 * 3 + 9 + 3 * 4 * 3)", 437)
          , ("5 * 9 * (7 * 3 * 3 + 9 * 3 + (8 + 6 * 4))", 12240)
          , ("((2 + 4 * 9) * (6 + 9 * 8 + 6) + 6) + 2 + 4 * 2", 13632)
          ]
          $ \(expr, res) ->
            it (Text.unpack expr) $
              (eval <$> parse1 expr) `shouldBe` Right res

    describe "part 2" $ do
      describe "samples" $
        forM_
          [ ("1 + (2 * 3) + (4 * (5 + 6))", 51)
          , ("2 * 3 + (4 * 5)", 46)
          , ("5 + (8 * 3 + 9 + 3 * 4 * 3)", 1445)
          , ("5 * 9 * (7 * 3 * 3 + 9 * 3 + (8 + 6 * 4))", 669060)
          , ("((2 + 4 * 9) * (6 + 9 * 8 + 6) + 6) + 2 + 4 * 2", 23340)
          ]
          $ \(expr, res) ->
            it (Text.unpack expr) $
              (eval <$> parse2 expr) `shouldBe` Right res