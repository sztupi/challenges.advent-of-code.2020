module Day03Spec
  ( spec,
  )
where

import qualified Data.Set as Set
import Data.Text (Text)
import Day03
import Test.Hspec
import Text.InterpolatedString.QM (qnb)

fullExample :: Text
fullExample =
  [qnb|..##.......
       #...#...#..
       .#....#..#.
       ..#.#...#.#
       .#...##..#.
       ..#.##.....
       .#.#.#....#
       .#........#
       #.##...#...
       #...##....#
       .#..#...#.#|]

smallExample :: Text
smallExample =
  [qnb|..##.
       #...#
       .#...
       ..#.#|]

spec :: Spec
spec =
  describe "day03" $ do
    describe "parse map" $ do
      it "example" $
        parseMap smallExample
          `shouldBe` Trees
            { coords =
                Set.fromList
                  [ (2, 0),
                    (3, 0),
                    (0, 1),
                    (4, 1),
                    (1, 2),
                    (2, 3),
                    (4, 3)
                  ],
              width = 5,
              height = 4
            }

    describe "count hits" $ do
      it "example" $
        countTreeHit (3, 1) (parseMap fullExample) `shouldBe` 7