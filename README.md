# Advent of Code 2020

# build / run / test

Prerequisites:

    $ cabal install hspec-discover

Using `cabal`:

    $ cabal build

    $ cabal test

    $ cabal run aoc2020 <day> <challenge>

    e.g.

    $ stack exec aoc2020 2 1

Run all programs:

    $ (source test.sh; run_all)
