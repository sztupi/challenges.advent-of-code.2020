#!/usr/bin/env bash

solved=14

function run_all() {
  for (( i = 1 ; i <= $solved ; i++ )); do
    for j in 1 2; do
      echo "day $i ch $j"
      cabal run -v0 aoc2020 $i $j
    done
  done
}

function gen_solution() {
  run_all > solutions.txt
}

function test() {
  diff -u solutions.txt <(run_all)
}
