module Main where

import qualified Data.Text.IO as Text.IO
import Day
import qualified Day01
import qualified Day02
import qualified Day03
import qualified Day04
import qualified Day05
import qualified Day06
import qualified Day07
import qualified Day08
import qualified Day09
import qualified Day10
import qualified Day11
import qualified Day12
import qualified Day13
import qualified Day14
import qualified Day15
import qualified Day16
import qualified Day17
import qualified Day18
import qualified Day19
import qualified Day20
import qualified Day21
import qualified Day22
import qualified Day23
import qualified Day24
import qualified Day25

import System.Environment as Env
import Text.Printf (printf)

main :: IO ()
main = do
  [read -> dayNum, read -> challengeNum] <- Env.getArgs
  let day = case dayNum :: Int of
        1 -> Day01.day
        2 -> Day02.day
        3 -> Day03.day
        4 -> Day04.day
        5 -> Day05.day
        6 -> Day06.day
        7 -> Day07.day
        8 -> Day08.day
        9 -> Day09.day
        10 -> Day10.day
        11 -> Day11.day
        12 -> Day12.day
        13 -> Day13.day
        14 -> Day14.day
        15 -> Day15.day
        16 -> Day16.day
        17 -> Day17.day
        18 -> Day18.day
        19 -> Day19.day
        20 -> Day20.day
        21 -> Day21.day
        22 -> Day22.day
        23 -> Day23.day
        24 -> Day24.day
        25 -> Day25.day
        _ -> error "nope, no such day yet"

      challenge = case challengeNum of
        1 -> challengeOne day
        2 -> challengeTwo day
        _ -> error "nope, no such challenge on that day"

      inputFileName = printf "inputs/day%02d.txt" dayNum

  input <- Text.IO.readFile inputFileName

  Text.IO.putStrLn $ challenge input
