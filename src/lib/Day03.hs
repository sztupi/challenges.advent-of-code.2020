module Day03 where

import Data.List
import Data.Maybe
import Data.Set (Set)
import qualified Data.Set as Set
import Data.Text (Text)
import qualified Data.Text as Text
import Day (Day (Day))

day :: Day
day = Day challengeOne challengeTwo

challengeOne :: Text -> Text
challengeOne = Text.pack . show . countTreeHit (3, 1) . parseMap

challengeTwo :: Text -> Text
challengeTwo input =
  let trees = parseMap input
      solution = product $ fmap (`countTreeHit` trees) [(1, 1), (3, 1), (5, 1), (7, 1), (1, 2)]
   in Text.pack . show $ solution

type Coord = (Int, Int)

type DCoord = (Int, Int)

data Trees = Trees
  { coords :: Set Coord,
    width :: Int,
    height :: Int
  }
  deriving (Eq, Show)

parseMap :: Text -> Trees
parseMap mapText =
  let lines = Text.lines mapText
      height = length lines
      width = maybe 0 Text.length $ listToMaybe lines
      coords = Set.fromList $ do
        (y, line) <- [0 ..] `zip` lines
        (x, _) <- filter ((== '#') . snd) $ [0 ..] `zip` Text.unpack line
        return (x, y)
   in Trees {..}

countTreeHit :: DCoord -> Trees -> Int
countTreeHit (dx, dy) Trees {..} =
  let pos = unfoldr (\(x, y) -> Just ((x, y), ((x + dx) `mod` width, y + dy))) (0, 0)
      pos' = takeWhile (\(_, y) -> y < height) pos
      hits = filter (`Set.member` coords) pos'
   in length hits