module Day11 where

import Control.Applicative
import Data.Map (Map)
import qualified Data.Map.Strict as Map
import Data.Maybe (catMaybes)
import Data.Text (Text)
import qualified Data.Text as Text
import Day (Day (Day))

day :: Day
day = Day challengeOne challengeTwo

challengeOne :: Text -> Text
challengeOne = Text.pack . show . countOccupied . findStable simpleStep . parseInput

challengeTwo :: Text -> Text
challengeTwo = Text.pack . show . countOccupied . findStable visibleStep . parseInput

data Seat = Available | Occupied
  deriving (Eq, Ord, Show)

data Sparse a = Sparse
  { sArea :: (Int, Int)
  , sPlaces :: Map (Int, Int) a
  }
  deriving (Eq)

instance Show (Sparse Seat) where
  show = Text.unpack . display

display :: Sparse Seat -> Text
display Sparse{sArea = (sx, sy), ..} =
  Text.unlines $
    flip map [0 .. sy -1] $ \y ->
      Text.pack $
        flip map [0 .. sx -1] $ \x ->
          displaySeat . Map.lookup (x, y) $ sPlaces
 where
  displaySeat :: Maybe Seat -> Char
  displaySeat Nothing = '.'
  displaySeat (Just Available) = 'L'
  displaySeat (Just Occupied) = '#'

parseInput :: Text -> Sparse Seat
parseInput input =
  let place ch = case ch of
        'L' -> Just Available
        '#' -> Just Occupied
        _ -> Nothing
      sPlaces =
        Map.fromList . catMaybes $
          [ ((x, y),) <$> place c
          | (y, line) <- [0 ..] `zip` Text.lines input
          , (x, c) <- [0 ..] `zip` Text.unpack line
          ]
      sArea = (Text.length . head . Text.lines $ input, length . Text.lines $ input)
   in Sparse{..}

runSteps :: Step -> Sparse Seat -> [Sparse Seat]
runSteps = iterate

directions :: [(Int, Int)]
directions = [(dx, dy) | dx <- [-1 .. 1], dy <- [-1 .. 1], dx /= 0 || dy /= 0]

neighbours :: (Int, Int) -> [(Int, Int)]
neighbours (x, y) = [(x + dx, y + dy) | (dx, dy) <- directions]

type Step = Sparse Seat -> Sparse Seat

simpleStep :: Step
simpleStep sp@Sparse{..} =
  sp
    { sPlaces = flip Map.mapWithKey sPlaces $ \(x, y) state ->
        let occupiedCount = length . filter (== Just Occupied) . fmap (`Map.lookup` sPlaces) . neighbours $ (x, y)
         in case (state, occupiedCount) of
              (Available, 0) -> Occupied
              (Available, _) -> Available
              (Occupied, n) | n >= 4 -> Available
              (Occupied, _) -> Occupied
    }

visibleStep :: Step
visibleStep sp@Sparse{sArea = (sx, sy), ..} =
  sp
    { sPlaces = flip Map.mapWithKey sPlaces $ \(x, y) state ->
        let occupiedCount = length . filter (== Occupied) . visibleSeats $ (x, y)
         in case (state, occupiedCount) of
              (Available, 0) -> Occupied
              (Available, _) -> Available
              (Occupied, n) | n >= 5 -> Available
              (Occupied, _) -> Occupied
    }
 where
  visibleSeats :: (Int, Int) -> [Seat]
  visibleSeats coord = catMaybes [findNextSeat coord dir | dir <- directions]

  findNextSeat :: (Int, Int) -> (Int, Int) -> Maybe Seat
  findNextSeat (x, y) _ | x < 0 || x >= sx || y < 0 || y >= sy = Nothing
  findNextSeat (x, y) (dx, dy) =
    let (x', y') = (x + dx, y + dy)
     in Map.lookup (x', y') sPlaces <|> findNextSeat (x', y') (dx, dy)

findStable :: Step -> Sparse Seat -> Sparse Seat
findStable step input =
  let next = step input
   in if next == input then input else findStable step next

countOccupied :: Sparse Seat -> Int
countOccupied = length . filter (== Occupied) . Map.elems . sPlaces