module Circle where

import Data.Foldable
import Data.Sequence (Seq ((:<|), (:|>)), (<|), (><), (|>))
import qualified Data.Sequence as Seq

data Circle a
  = Circle a (Seq a)
  deriving (Show, Eq, Ord)

cw :: Circle a -> Circle a
cw (Circle f (a :<| as)) = Circle a (as |> f)

curr :: Circle a -> a
curr (Circle f _) = f

takeCW :: Int -> Circle a -> ([a], Circle a)
takeCW n (Circle f as) = (toList $ Seq.take n as, Circle f (Seq.drop n as))

findCW :: Eq a => a -> Circle a -> Circle a
findCW d c@(Circle f _) | f == d = c
findCW d (Circle _ rs) =
  let (rs1, _ :<| rs2) = Seq.breakl (== d) rs
   in Circle d (rs2 >< rs1)

insertCW :: Eq a => a -> [a] -> Circle a -> Circle a
insertCW = insertCW2

insertCW1 :: Eq a => a -> [a] -> Circle a -> Circle a
insertCW1 d as (Circle f rs) | f == d = Circle f (Seq.fromList as >< rs)
insertCW1 d as (Circle f rs) =
  let (rs1, _ :<| rs2) = Seq.breakl (== d) rs
   in Circle f (rs1 >< d <| Seq.fromList as >< rs2)

insertCW2 :: forall a. Eq a => a -> [a] -> Circle a -> Circle a
insertCW2 d (Seq.fromList -> as) (Circle f rs) | f == d = Circle f (as >< rs)
insertCW2 d (Seq.fromList -> as) (Circle f rs) =
  let rs' = insertRs2 (rs, Seq.empty) (rs, Seq.empty)
   in Circle f rs'
 where
  insertRs2 :: (Seq a, Seq a) -> (Seq a, Seq a) -> Seq a
  insertRs2 (a :<| s1, r1) _ | a == d = r1 >< (d <| as) >< s1
  insertRs2 _ (s2 :|> a, r2) | a == d = s2 >< (d <| as) >< r2
  insertRs2 (a1 :<| s1, r1) (s2 :|> a2, r2) = insertRs2 (s1, r1 |> a1) (s2, a2 <| r2)

putCW :: [a] -> Circle a -> Circle a
putCW as (Circle f rs) = Circle f (Seq.fromList as >< rs)