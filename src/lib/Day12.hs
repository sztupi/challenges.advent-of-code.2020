module Day12 where

import Data.Either.Extra (mapLeft)
import Data.List (foldl')
import Data.Text (Text)
import qualified Data.Text as Text
import Data.Void (Void)
import Day (Day (Day))
import Text.Megaparsec
import Text.Megaparsec.Char
import Text.Megaparsec.Char.Lexer

day :: Day
day = Day challengeOne challengeTwo

challengeOne :: Text -> Text
challengeOne = Text.pack . show . fmap ((\Ferry{fPos = (x, y), ..} -> abs x + abs y) . (`runInstructions` ferryStart)) . parseInput

challengeTwo :: Text -> Text
challengeTwo = Text.pack . show . fmap ((\WayFerry{wfPos = (x, y), ..} -> abs x + abs y) . (`runWaypoints` wferryStart)) . parseInput

data Instruction
  = INorth Int
  | ISouth Int
  | IEast Int
  | IWest Int
  | ILeft Int
  | IRight Int
  | IForward Int
  deriving (Eq, Ord, Show)

data Heading = HEast | HSouth | HWest | HNorth
  deriving (Eq, Ord, Show)

turnLeft, turnRight :: Int -> Heading -> Heading
turnLeft n | n >= 0 = foldl' (.) id (replicate n turnL)
turnLeft n = turnRight (- n)
turnRight n | n >= 0 = foldl' (.) id (replicate n turnR)
turnRight n = turnLeft (- n)

turnL, turnR :: Heading -> Heading
turnL HEast = HNorth
turnL HNorth = HWest
turnL HWest = HSouth
turnL HSouth = HEast
turnR = turnL . turnL . turnL

data Ferry = Ferry
  { fHead :: Heading
  , fPos :: (Int, Int)
  }
  deriving (Eq, Ord, Show)

data WayFerry = WayFerry
  { wfPos :: (Int, Int)
  , wfWaypoint :: (Int, Int)
  }
  deriving (Eq, Ord, Show)

ferryStart :: Ferry
ferryStart = Ferry HEast (0, 0)

wferryStart :: WayFerry
wferryStart = WayFerry (0, 0) (10, -1)

runInstructions :: [Instruction] -> Ferry -> Ferry
runInstructions = flip (foldl' runInstruction)

runInstruction :: Ferry -> Instruction -> Ferry
runInstruction ferry@Ferry{fPos = (x, y), ..} = \case
  INorth n -> ferry{fPos = (x, y - n)}
  ISouth n -> ferry{fPos = (x, y + n)}
  IEast n -> ferry{fPos = (x + n, y)}
  IWest n -> ferry{fPos = (x - n, y)}
  ILeft n -> ferry{fHead = turnLeft (n `div` 90) fHead}
  IRight n -> ferry{fHead = turnRight (n `div` 90) fHead}
  IForward n -> case fHead of
    HEast -> runInstruction ferry (IEast n)
    HWest -> runInstruction ferry (IWest n)
    HNorth -> runInstruction ferry (INorth n)
    HSouth -> runInstruction ferry (ISouth n)

runWaypoints :: [Instruction] -> WayFerry -> WayFerry
runWaypoints = flip (foldl' runWInstruction)

runWInstruction :: WayFerry -> Instruction -> WayFerry
runWInstruction ferry@WayFerry{wfWaypoint = wfWaypoint@(x, y), wfPos = (px, py)} = \case
  INorth n -> ferry{wfWaypoint = (x, y - n)}
  ISouth n -> ferry{wfWaypoint = (x, y + n)}
  IEast n -> ferry{wfWaypoint = (x + n, y)}
  IWest n -> ferry{wfWaypoint = (x - n, y)}
  ILeft n -> ferry{wfWaypoint = turnWLeft (n `div` 90) wfWaypoint}
  IRight n -> ferry{wfWaypoint = turnWRight (n `div` 90) wfWaypoint}
  IForward n -> ferry{wfPos = (px + x * n, py + y * n)}

turnWLeft, turnWRight :: Int -> (Int, Int) -> (Int, Int)
turnWLeft n | n >= 0 = foldl' (.) id (replicate n turnWL)
turnWLeft n = turnWRight (- n)
turnWRight n | n >= 0 = foldl' (.) id (replicate n turnWR)
turnWRight n = turnWLeft (- n)

turnWL, turnWR :: (Int, Int) -> (Int, Int)
turnWL (x, y) = (y, -x)
turnWR (x, y) = (-y, x)

parseInput :: Text -> Either String [Instruction]
parseInput = mapLeft show . runParser instructionsP "<none>"

type P = Parsec Void Text

instructionsP :: P [Instruction]
instructionsP = instructionP `sepBy` newline

instructionP :: P Instruction
instructionP =
  choice
    [ INorth <$ string "N"
    , ISouth <$ string "S"
    , IEast <$ string "E"
    , IWest <$ string "W"
    , ILeft <$ string "L"
    , IRight <$ string "R"
    , IForward <$ string "F"
    ]
    <*> decimal
