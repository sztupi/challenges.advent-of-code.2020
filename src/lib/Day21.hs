module Day21 where

import Data.Bifunctor
import Data.Either.Extra (mapLeft, partitionEithers)
import Data.List (sortOn)
import qualified Data.Map.Strict as Map
import Data.Set (Set)
import qualified Data.Set as Set
import Data.Text (Text)
import qualified Data.Text as Text
import Data.Void (Void)
import Day (Day (Day))
import Text.Megaparsec hiding (parse)
import Text.Megaparsec.Char hiding (space)
import qualified Text.Megaparsec.Char.Lexer as L

day :: Day
day = Day challengeOne challengeTwo

challengeOne :: Text -> Text
challengeOne = Text.pack . show . fmap findNonAllergenOccurences . parseInput

challengeTwo :: Text -> Text
challengeTwo = Text.pack . show . fmap (Text.intercalate "," . fmap unIngredient . inferAllergens) . parseInput

newtype Allergen = Allergen Text
  deriving (Eq, Ord, Show)
newtype Ingredient = Ingredient {unIngredient :: Text}
  deriving (Eq, Ord, Show)
type Input = [(Set Ingredient, Set Allergen)]

parseInput :: Text -> Either Text Input
parseInput = mapLeft (Text.pack . show) . runParser inputP "<none>"

findNonAllergens :: Input -> Set Ingredient
findNonAllergens input =
  let allIngredients = Set.unions $ fst <$> input
      allAllergens = Set.unions $ snd <$> input
      allergens =
        Map.fromList
          [ (allergen, foldl1 Set.intersection foods)
          | allergen <- Set.toList allAllergens
          , let foods = fst <$> filter (Set.member allergen . snd) input
          , not . null $ foods
          ]
      nonAllergens = allIngredients Set.\\ Set.unions (Map.elems allergens)
   in nonAllergens

findNonAllergenOccurences :: Input -> Int
findNonAllergenOccurences input =
  let nonAllergens = findNonAllergens input
   in sum $ Set.size . Set.intersection nonAllergens . fst <$> input

inferAllergens :: Input -> [Ingredient]
inferAllergens input =
  let allAllergens = Set.unions $ snd <$> input
      allergens =
        [ (allergen, foldl1 Set.intersection foods)
        | allergen <- Set.toList allAllergens
        , let foods = fst <$> filter (Set.member allergen . snd) input
        , not . null $ foods
        ]

      reduced = reduceAllergens [] allergens
   in fmap fst . sortOn snd $ reduced
 where
  reduceAllergens :: [(Ingredient, Allergen)] -> [(Allergen, Set Ingredient)] -> [(Ingredient, Allergen)]
  reduceAllergens found allergenSets =
    let (singletons, multiples) =
          partitionEithers $ do
            (a, is) <- allergenSets
            return $ case Set.toList is of
              [i] -> Left (i, a)
              _ -> Right (a, is)
     in if null singletons
          then found
          else
            let toRemove = Set.fromList $ fst <$> singletons
             in reduceAllergens (singletons <> found) (second (Set.\\ toRemove) <$> multiples)

type P = Parsec Void Text

inputP :: P Input
inputP = some ruleP

space :: P ()
space = L.space space1 empty empty

lexeme :: P a -> P a
lexeme = L.lexeme space

symbol :: Text -> P Text
symbol = L.symbol space

ruleP :: P (Set Ingredient, Set Allergen)
ruleP = do
  ingredients <- Set.fromList . fmap (Ingredient . Text.pack) <$> some (lexeme (some alphaNumChar))
  allergens <-
    between
      (symbol "(")
      (symbol ")")
      ( symbol "contains"
          *> (Set.fromList . fmap (Allergen . Text.pack) <$> lexeme (some alphaNumChar) `sepBy` symbol ",")
      )
  return (ingredients, allergens)
