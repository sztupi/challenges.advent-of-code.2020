module Day24 where

import Data.Either.Extra (mapLeft)
import Data.List (foldl')
import Data.Set (Set)
import qualified Data.Set as Set
import Data.Text (Text)
import qualified Data.Text as Text
import Data.Void (Void)
import Day (Day (Day))
import Text.Megaparsec
import Text.Megaparsec.Char
import Data.Bifunctor

day :: Day
day = Day challengeOne challengeTwo

challengeOne :: Text -> Text
challengeOne = Text.pack . show . fmap findBlackTileCount . parseInput

challengeTwo :: Text -> Text
challengeTwo = Text.pack . show . fmap (runSteps 100) . parseInput

data Dir
  = NE
  | E
  | SE
  | SW
  | W
  | NW
  deriving (Show, Eq, Ord, Bounded, Enum)

dirs :: [Dir]
dirs = [minBound .. maxBound]

findBlackTileCount :: [[Dir]] -> Int
findBlackTileCount =
  Set.size . dirssToTiles

dirssToTiles :: [[Dir]] -> Set (Int, Int)
dirssToTiles = foldl' toggle Set.empty . fmap dirsToCoord

dirsToCoord :: [Dir] -> (Int, Int)
dirsToCoord = foldl' (\(x1, y1) (x, y) -> (x + x1, y + y1)) (0, 0) . fmap dirToDCoord

--         x
--      o-->
--     /
-- y |_

dirToDCoord :: Dir -> (Int, Int)
dirToDCoord = \case
  NE -> (0, -1)
  E -> (1, 0)
  SE -> (1, 1)
  SW -> (0, 1)
  W -> (-1, 0)
  NW -> (-1, -1)

toggle :: Ord a => Set a -> a -> Set a
toggle s a | a `Set.member` s = Set.delete a s
toggle s a = Set.insert a s

runSteps :: Int -> [[Dir]] -> Int
runSteps n dirss =
  let start = dirssToTiles dirss
      end = foldl' (.) id (replicate n step) start
   in Set.size end

neighs :: (Int, Int) -> Set (Int, Int)
neighs (x, y) = Set.fromList $ fmap (bimap (x +) (y +) . dirToDCoord) dirs

bneighs :: (Int,Int) -> Set (Int,Int) -> Set (Int,Int)
bneighs c blacks = neighs c `Set.intersection` blacks

step :: Set (Int, Int) -> Set (Int, Int)
step blacks =
  let whites = Set.unions $ neighs <$> Set.toList blacks
      blacksToFlip = Set.filter (\c -> Set.size (bneighs c blacks) /= 1) blacks
      whitesToFlip = Set.filter (\c -> Set.size (bneighs c blacks) == 2) whites
   in foldl' toggle blacks (Set.toList blacksToFlip <> Set.toList whitesToFlip)

parseInput :: Text -> Either Text [[Dir]]
parseInput = mapLeft (Text.pack . show) . runParser tilesP "<none>"

type P = Parsec Void Text

tilesP :: P [[Dir]]
tilesP = tileP `sepBy` newline

tileP :: P [Dir]
tileP =
  some $
    choice
      [ E <$ string "e"
      , W <$ string "w"
      , NE <$ string "ne"
      , NW <$ string "nw"
      , SE <$ string "se"
      , SW <$ string "sw"
      ]
