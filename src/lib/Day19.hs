module Day19 where

import Control.Monad (ap, void)
import Data.Bifunctor
import Data.Either.Extra (mapLeft)
import Data.Map (Map)
import qualified Data.Map.Strict as Map
import Data.Text (Text)
import qualified Data.Text as Text
import Data.Void (Void)
import Day (Day (Day))
import Text.Megaparsec hiding (parse)
import Text.Megaparsec.Char hiding (space)
import qualified Text.Megaparsec.Char.Lexer as L

day :: Day
day = Day challengeOne challengeTwo

challengeOne :: Text -> Text
challengeOne = Text.pack . show . fmap length . uncurry findMatchingMessages . parseInput

challengeTwo :: Text -> Text
challengeTwo = Text.pack . show . fmap length . uncurry findMatchingMessages . first (fmap rulesUpdate) . parseInput

type Rules r = Map r (Rule r)

data Rule r
  = RChar Char
  | REither [Rule r]
  | RSeq [Rule r]
  | RSub r
  deriving (Show, Eq, Ord, Functor, Foldable, Traversable)

instance Applicative Rule where
  pure = return
  (<*>) = ap

instance Monad Rule where
  return = RSub

  (>>=) :: Rule a -> (a -> Rule b) -> Rule b
  (RSub r) >>= k = k r
  (RChar c) >>= _ = RChar c
  (REither rs) >>= k = REither ((>>= k) <$> rs)
  (RSeq rs) >>= k = RSeq ((>>= k) <$> rs)

rulesUpdate :: Rules Int -> Rules Int
rulesUpdate =
  Map.union $
    Map.fromList
      [ (8, REither [RSub 42, RSeq [RSub 42, RSub 8]])
      , (11, REither [RSeq [RSub 42, RSub 31], RSeq [RSub 42, RSub 11, RSub 31]])
      ]

parseInput :: Text -> ParsedInput
parseInput input =
  let [ruleText, messages] = Text.splitOn "\n\n" input
   in (parseRules ruleText, Text.lines messages)

type ParsedInput = (Either Text (Rules Int), [Text])

flattenRules :: Rules Int -> Either Text (Rule ())
flattenRules rules = do
  r0I <- case Map.lookup 0 rules of
    Nothing -> Left "no root rule found"
    Just r0I -> Right r0I

  return $ void $ r0I >>= lookupRule
 where
  lookupRule :: Int -> Rule Int
  lookupRule i =
    case Map.lookup i rules of
      Just r@(RChar _) -> r
      Just (RSub i') -> lookupRule i'
      Just r -> r >>= lookupRule
      Nothing -> error "rule not found"

findMatchingMessages :: Either Text (Rules Int) -> [Text] -> Either Text [Text]
findMatchingMessages eRules messages =
  do
    rule <- eRules >>= flattenRules
    return $ filter (elem "" . ruleMatch rule . Text.unpack) messages

ruleMatch :: Rule () -> String -> [String]
ruleMatch (RSub _) _ = []
ruleMatch (RSeq []) s = pure s
ruleMatch (RSeq (r : rs)) s = ruleMatch r s >>= \s' -> ruleMatch (RSeq rs) s'
ruleMatch (REither rs) s = rs >>= (`ruleMatch` s)
ruleMatch _ [] = []
ruleMatch (RChar c') (c : cs) | c == c' = pure cs
ruleMatch (RChar _) _ = []

type P = Parsec Void Text

parseRules :: Text -> Either Text (Rules Int)
parseRules = mapLeft (Text.pack . show) . runParser rulesP "<none>"

space :: P ()
space = L.space (void $ some $ char ' ') empty empty

lexeme :: P a -> P a
lexeme = L.lexeme space

symbol :: Text -> P Text
symbol = L.symbol space

rulesP :: P (Rules Int)
rulesP = Map.fromList <$> rowP `sepBy` newline

rowP :: P (Int, Rule Int)
rowP = do
  rId <- lexeme L.decimal
  _ <- symbol ":"
  rule <- ruleP
  return (rId, rule)

ruleP :: P (Rule Int)
ruleP =
  choice
    [ RChar <$> between (symbol "\"") (symbol "\"") alphaNumChar
    , do
        lists <- some (RSub <$> lexeme L.decimal) `sepBy` symbol "|"
        case lists of
          [rs] -> return $ RSeq rs
          rss@(_ : _) -> return $ REither (RSeq <$> rss)
          _ -> error "bad input or bug"
    ]