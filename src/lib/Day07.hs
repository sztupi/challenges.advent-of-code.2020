{-# LANGUAGE TupleSections #-}

module Day07 where

import Data.Char (isSpace)
import Data.Either.Extra (mapLeft)
import Data.List (unfoldr)
import Data.Map (Map)
import qualified Data.Map as Map
import Data.Maybe (fromMaybe)
import Data.Set (Set)
import qualified Data.Set as Set
import Data.Text (Text)
import qualified Data.Text as Text
import Data.Void (Void)
import Day (Day (Day))
import Text.Megaparsec
import Text.Megaparsec.Char
import Text.Megaparsec.Char.Lexer

day :: Day
day = Day challengeOne challengeTwo

challengeOne :: Text -> Text
challengeOne = Text.pack . show . countPossibleContainerColors "shiny gold"

challengeTwo :: Text -> Text
challengeTwo = Text.pack . show . countContainedBags "shiny gold"

type BagName = Text

type BagRules = Map BagName BagRule

type BagRule = Map BagName Int

type BagParents = Map BagName (Set BagName)

countPossibleContainerColors :: BagName -> Text -> Either String Int
countPossibleContainerColors bagName = fmap (Set.size . (`findAllParents` bagName) . directParents) . parseRules

countContainedBags :: BagName -> Text -> Either String Int
countContainedBags bagName = fmap ((\c -> c - 1) . countBags bagName) . parseRules

directParents :: BagRules -> BagParents
directParents = Map.unionsWith Set.union . fmap ruleToParents . Map.toList
  where
    ruleToParents :: (BagName, BagRule) -> Map BagName (Set BagName)
    ruleToParents (parent, rules) =
      Map.fromList $ (,Set.singleton parent) <$> Map.keys rules

findAllParents :: BagParents -> BagName -> Set BagName
findAllParents parents bagName = Set.unions $ unfoldr nextGen (Set.singleton bagName)
  where
    nextGen :: Set BagName -> Maybe (Set BagName, Set BagName)
    nextGen bags | Set.null bags = Nothing
    nextGen bags =
      let s = Set.unions $ Set.map (fromMaybe Set.empty . (`Map.lookup` parents)) bags
       in Just (s, s)

countBags :: BagName -> BagRules -> Int
countBags bagName rules =
  let deepCountMap :: Map BagName Int
      deepCountMap = Map.fromList $ (\bn -> (bn, countBags' bn)) <$> Map.keys rules

      countBags' :: BagName -> Int
      countBags' bn =
        let children = fromMaybe Map.empty $ Map.lookup bn rules
         in 1 + sum ((\(child, count) -> count * fromMaybe 0 (Map.lookup child deepCountMap)) <$> Map.toList children)
   in countBags' bagName

parseRules :: Text -> Either String BagRules
parseRules = mapLeft show . runParser rulesP "<none>"

type P = Parsec Void Text

rulesP :: P BagRules
rulesP = Map.fromList <$> ruleP `sepBy` eol

ruleP :: P (BagName, BagRule)
ruleP = do
  bagName <- bagNameP
  _ <- string " contain "
  bagRule <-
    choice
      [ Map.empty <$ string "no other bags",
        Map.fromList <$> bagRulePartP `sepBy` string ", "
      ]
  _ <- char '.'
  return (bagName, bagRule)

bagRulePartP :: P (BagName, Int)
bagRulePartP = do
  bags <- decimal
  _ <- spaceChar
  bagName <- bagNameP
  return (bagName, bags)

wordP :: P Text
wordP = takeWhile1P Nothing (not . isSpace)

bagNameP :: P BagName
bagNameP =
  (mconcat <$> sequence [wordP, string " ", wordP])
    <* choice [string " bags", string " bag"]