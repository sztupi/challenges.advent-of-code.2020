module Day10 where

import qualified Data.List as List
import qualified Data.Set as Set
import Data.Text (Text)
import qualified Data.Text as Text
import qualified Data.Text.Read as Text.Read
import Day (Day (Day))

day :: Day
day = Day challengeOne challengeTwo

challengeOne :: Text -> Text
challengeOne = Text.pack . show . fmap multChainDiffs . traverse (fmap fst . Text.Read.decimal) . Text.lines

challengeTwo :: Text -> Text
challengeTwo = Text.pack . show . fmap numberOfValidChains . traverse (fmap fst . Text.Read.decimal) . Text.lines

multChainDiffs :: [Int] -> Int
multChainDiffs adapters =
  let chain = 0 : List.sort adapters <> [maximum adapters + 3]
      diffs = uncurry (-) <$> (tail chain `zip` chain)
   in length (filter (== 1) diffs) * length (filter (== 3) diffs)

numberOfValidChains :: [Int] -> Int
numberOfValidChains adapters =
  let items = Set.fromList (0 : adapters)
      validWaysArr = [1] <> [validWays n | n <- [1 ..]]
      validWays n =
        sum
          [ if (n - d) `Set.member` items then validWaysArr !! (n - d) else 0
          | d <- [3, 2, 1]
          ]
   in validWays (maximum items)