module Day22 where

import Data.Bifunctor
import Data.Either.Extra (mapLeft)
import Data.Foldable
import Data.Sequence (Seq ((:<|)), (|>))
import qualified Data.Sequence as Seq
import qualified Data.Set as Set
import Data.Text (Text)
import qualified Data.Text as Text
import Data.Void (Void)
import Day (Day (Day))
import Text.Megaparsec hiding (parse)
import Text.Megaparsec.Char hiding (space)
import qualified Text.Megaparsec.Char.Lexer as L

day :: Day
day = Day challengeOne challengeTwo

challengeOne :: Text -> Text
challengeOne = Text.pack . show . fmap (scoreGame . runGame) . parseInput

challengeTwo :: Text -> Text
challengeTwo = Text.pack . show . fmap (second scoreGame . runRecGame) . parseInput

type Card = Int
type Game = ([Card], [Card])

scoreGame :: Game -> (Int, Int)
scoreGame (p1, p2) = (score p1, score p2)
 where
  score = sum . fmap (uncurry (*)) . zip [1 ..] . reverse

runGame :: Game -> Game
runGame (p1, p2) = runGame' (Seq.fromList p1) (Seq.fromList p2)
 where
  runGame' :: Seq Card -> Seq Card -> Game
  runGame' cs1 Seq.Empty = (toList cs1, [])
  runGame' Seq.Empty cs2 = ([], toList cs2)
  runGame' (c1 :<| cs1) (c2 :<| cs2) =
    if c1 > c2
      then runGame' (cs1 |> c1 |> c2) cs2
      else runGame' cs1 (cs2 |> c2 |> c1)

data Player = P1 | P2
  deriving (Eq, Ord, Show)

runRecGame :: Game -> (Player, Game)
runRecGame (h1', h2') = g (Set.empty, Set.empty) (Seq.fromList h1') (Seq.fromList h2')
 where
  g _ cs1 Seq.Empty = (P1, (toList cs1, []))
  g _ Seq.Empty cs2 = (P2, ([], toList cs2))
  g (p1, p2) cs1 cs2 | cs1 `Set.member` p1 || cs2 `Set.member` p2 = (P1, (toList cs1, toList cs2))
  g (p1, p2) h1@(c1 :<| cs1) h2@(c2 :<| cs2) =
    let prevs = (Set.insert h1 p1, Set.insert h2 p2)
        winner
          | c1 <= Seq.length cs1 && c2 <= Seq.length cs2 =
            fst $ runRecGame (take c1 $ toList cs1, take c2 $ toList cs2)
          | c1 > c2 = P1
          | otherwise = P2
     in case winner of
          P1 -> g prevs (cs1 |> c1 |> c2) cs2
          P2 -> g prevs cs1 (cs2 |> c2 |> c1)

parseInput :: Text -> Either Text Game
parseInput = mapLeft (Text.pack . show) . runParser inputP "<none>"

type P = Parsec Void Text

space :: P ()
space = L.space space1 empty empty

lexeme :: P a -> P a
lexeme = L.lexeme space

symbol :: Text -> P Text
symbol = L.symbol space

inputP :: P ([Card], [Card])
inputP = do
  (,) <$> (symbol "Player 1:" *> cardsP) <*> (symbol "Player 2:" *> cardsP)

cardsP :: P [Card]
cardsP = some (lexeme L.decimal)
