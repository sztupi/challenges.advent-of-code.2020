module Day14 where

import Data.Bits ((.&.), (.|.))
import qualified Data.Bits as Bits
import Data.Either.Extra (mapLeft)
import Data.List (foldl')
import Data.Map (Map)
import qualified Data.Map as Map
import Data.Set (Set)
import qualified Data.Set as Set
import Data.Text (Text)
import qualified Data.Text as Text
import Data.Void (Void)
import Day (Day (Day))
import Text.Megaparsec
import Text.Megaparsec.Char
import Text.Megaparsec.Char.Lexer

day :: Day
day = Day challengeOne challengeTwo

challengeOne :: Text -> Text
challengeOne = Text.pack . show . fmap (sumValues . (`runProgram` initialState)) . parseInput

challengeTwo :: Text -> Text
challengeTwo = Text.pack . show . fmap (sumValues . (`runProgramV2` initialState)) . parseInput

newtype Program = Program
  { pInstructions :: [PInstruction]
  }
  deriving (Eq, Show)

data PInstruction
  = IAssign Integer Integer
  | ISetMask (Integer, Integer, Set Integer)
  deriving (Eq, Ord, Show)

data PState = PState
  { pMask :: (Integer, Integer, Set Integer)
  , pMem :: Map Integer Integer
  }
  deriving (Show, Eq)

initialState :: PState
initialState = PState (0, 0, Set.empty) Map.empty

runProgram :: Program -> PState -> PState
runProgram Program{..} init = foldl' runStep init pInstructions
 where
  runStep :: PState -> PInstruction -> PState
  runStep st@PState{..} (IAssign addr target) = st{pMem = Map.insert addr (maskedValue pMask target) pMem}
  runStep st@PState{..} (ISetMask mask) = st{pMask = mask}

  maskedValue :: (Integer, Integer, Set Integer) -> Integer -> Integer
  maskedValue (m1, m0, _mf) v = (m1 .|. v) .&. Bits.complement m0

runProgramV2 :: Program -> PState -> PState
runProgramV2 Program{..} init = foldl' runStep init pInstructions
 where
  runStep :: PState -> PInstruction -> PState
  runStep st@PState{..} (IAssign addr target) = st{pMem = foldl' (\mem maddr -> Map.insert maddr target mem) pMem (maskedAddrs pMask addr)}
  runStep st@PState{..} (ISetMask mask) = st{pMask = mask}

  maskedAddrs :: (Integer, Integer, Set Integer) -> Integer -> [Integer]
  maskedAddrs (m1, _m0, mf) addr =
    [ op (m1 .|. addr)
    | bits <- Set.toList (Set.powerSet mf)
    , let op =
            foldl'
              ( \acc bit ->
                  if bit `elem` bits
                    then acc . (.|. bit)
                    else acc . (.&. Bits.complement bit)
              )
              id
              (Set.toList mf)
    ]

sumValues :: PState -> Integer
sumValues = sum . Map.elems . pMem

parseInput :: Text -> Either Text Program
parseInput = mapLeft (Text.pack . show) . runParser programP "<none>"

type P = Parsec Void Text

programP :: P Program
programP = do
  pInstructions <- instructionP `sepBy` newline
  return Program{..}

instructionP :: P PInstruction
instructionP =
  choice
    [ string "mem[" *> (IAssign <$> decimal <*> (string "] = " *> decimal))
    , string "mask = " *> do
        chars <- many (choice [(1, 0, False) <$ char '1', (0, 1, False) <$ char '0', (0, 0, True) <$ char 'X'])
        return . ISetMask $
          foldl'
            ( \(acc1, acc0, accf) (b1, b0, bf) ->
                ( 2 * acc1 + b1
                , 2 * acc0 + b0
                , let accf' = Set.map (* 2) accf
                   in if bf
                        then Set.insert 1 accf'
                        else accf'
                )
            )
            (0, 0, Set.empty)
            chars
    ]