module Day15 where

import Data.Either.Extra (mapLeft)
import Data.Map (Map)
import qualified Data.Map.Strict as Map
import Data.Text (Text)
import qualified Data.Text as Text
import Data.Void (Void)
import Day (Day (Day))
import Text.Megaparsec
import Text.Megaparsec.Char
import Text.Megaparsec.Char.Lexer

day :: Day
day = Day challengeOne challengeTwo

challengeOne :: Text -> Text
challengeOne = Text.pack . show . fmap (findStep 2020) . parseInput

challengeTwo :: Text -> Text
challengeTwo = Text.pack . show . fmap (findStep 30000000) . parseInput

parseInput :: Text -> Either Text [Int]
parseInput = mapLeft (Text.pack . show) . runParser inputP "<none>"

{-# ANN findStep ("HLint: ignore Use uncurry" :: Text) #-}
findStep :: Int -> [Int] -> Int
findStep n start@(h : _) = find' 0 start (h, Map.empty)
 where
  find' :: Int -> [Int] -> (Int, Map Int Int) -> Int
  find' i _ prev | i == n = fst prev
  find' i (x : xs) prev = find' (i + 1) xs (x, Map.insert (fst prev) i $ snd prev)
  find' i [] prev = find' (i + 1) [] (maybe 0 (i -) $ Map.lookup (fst prev) (snd prev), Map.insert (fst prev) i $ snd prev)

type P = Parsec Void Text

inputP :: P [Int]
inputP = decimal `sepBy` char ','
