{-# LANGUAGE QuasiQuotes #-}

module Day02 where

import Data.Either.Extra (mapLeft)
import Data.Text (Text)
import qualified Data.Text as Text
import Data.Void (Void)
import Day (Day (Day))
import GHC.Generics (Generic)
import Text.InterpolatedString.Perl6 (qc)
import Text.Megaparsec hiding (Pos)
import Text.Megaparsec.Char
import Text.Megaparsec.Char.Lexer

day :: Day
day = Day challengeOne challengeTwo

challengeOne :: Text -> Text
challengeOne = Text.pack . show . countValidLines

challengeTwo :: Text -> Text
challengeTwo = Text.pack . show . countValidTobLines

data PwPolicy = PwPolicy
  { ppChar :: Char,
    ppCount :: (Int, Int)
  }
  deriving (Eq, Show, Generic)

data PwLine = PwLine
  { plPolicy :: PwPolicy,
    plPassword :: Text
  }
  deriving (Eq, Show, Generic)

countValidLines :: Text -> Either String Int
countValidLines = fmap (length . filter (== True)) . traverse (fmap verify . parseLine) . Text.lines

countValidTobLines :: Text -> Either String Int
countValidTobLines = fmap (length . filter (== True)) . traverse (fmap verifyTob . parseLine) . Text.lines

verify :: PwLine -> Bool
verify PwLine {plPolicy = PwPolicy {ppCount = (cmin, cmax), ..}, ..} =
  let charCount = length $ filter (== ppChar) (Text.unpack plPassword)
   in cmin <= charCount && cmax >= charCount

verifyTob :: PwLine -> Bool
verifyTob PwLine {plPolicy = PwPolicy {ppCount = (cmin, cmax), ..}, ..} =
  let at n = Text.head $ Text.drop (n - 1) plPassword
   in (at cmin == ppChar) /= (at cmax == ppChar)

lineToText :: PwLine -> Text
lineToText PwLine {plPolicy = PwPolicy {ppCount = (cmin, cmax), ..}, ..} =
  [qc|{cmin}-{cmax} {ppChar}: {plPassword}|]

parseLine :: Text -> Either String PwLine
parseLine input = mapLeft show $ runParser lineP "<none>" input

type P = Parsec Void Text

lineP :: P PwLine
lineP = do
  plPolicy <- policyP
  string ": "
  plPassword <- Text.pack <$> some alphaNumChar
  return PwLine {..}

policyP :: P PwPolicy
policyP = do
  cmin <- decimal
  char '-'
  cmax <- decimal
  char ' '
  ppChar <- alphaNumChar
  let ppCount = (cmin, cmax)
  return PwPolicy {..}