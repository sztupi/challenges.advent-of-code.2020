module Day23 where

import Circle
import Control.Monad.ST
import qualified Data.Char as Char
import Data.Foldable
import Data.STRef
import qualified Data.Sequence as Seq
import Data.Text (Text)
import qualified Data.Text as Text
import qualified Data.Vector.Unboxed.Mutable as VUM
import Day (Day (Day))
import qualified Debug.Trace as Debug

day :: Day
day = Day challengeOne challengeTwo

challengeOne :: Text -> Text
challengeOne = Text.intercalate "" . fmap (Text.pack . show) . runGame 100 . parseInput

challengeTwo :: Text -> Text
challengeTwo = Text.pack . show . uncurry (*) . play2 . parseInput

parseInput :: Text -> [Int]
parseInput = fmap Char.digitToInt . Text.unpack

type Game = Circle Int

gameFromList :: [Int] -> Game
gameFromList (a : as) = Circle a (Seq.fromList as)

gameToList :: Game -> [Int]
gameToList (Circle h (toList -> rs)) = h : rs

runGame :: Int -> [Int] -> [Int]
runGame n = gameToList . findCW 1 . runSteps n . gameFromList

runSteps :: Int -> Game -> Game
runSteps n g =
  -- foldl' (.) id (replicate n gameStep)
  runSteps' 0 g
 where
  (mn, mx) = bounds g

  runSteps' m g' | m == n = g'
  runSteps' m g' = runSteps' (m + 1) (Debug.trace (show m) $! gameStep (mn, mx) g')

bounds :: Game -> (Int, Int)
bounds (Circle f rs) = (minimum (f : toList rs), maximum (f : toList rs))

gameStep :: (Int, Int) -> Game -> Game
gameStep (mn, mx) c =
  let (hand, c') = takeCW 3 c
      f = curr c'
      dest = head $ filter (`notElem` hand) ([f -1, f -2 .. mn] <> [mx, mx -1 .. f + 1])
      c'' = insertCW dest hand c'
   in cw c''

play2 :: [Int] -> (Int, Int)
play2 input =
  fst $ runGame2 1000000 10000000 input

runGame2 :: Int -> Int -> [Int] -> ((Int, Int), [Int])
runGame2 maxCup steps initCups =
  let mx = maximum initCups
      input' = initCups <> [mx + 1 .. maxCup]
   in runST (gameST maxCup steps input')

gameST :: Int -> Int -> [Int] -> ST s ((Int, Int), [Int])
gameST maxCup steps cupsL = do
  cups <- VUM.new (maxCup + 1)
  let prev = last cupsL : cupsL
      next = tail cupsL <> cupsL

  forM_ (zip3 cupsL prev next) $ \(i, p, n) -> do
    VUM.write cups i (p, n)

  curr <- newSTRef (head cupsL)

  forM_ [1 .. steps] $ \_ ->
    step curr cups

  (_, a) <- VUM.read cups 1
  (_, b) <- VUM.read cups a

  cupsL' <- listFromIdx 1 9 cups []
  -- cups' <- VU.freeze cups

  return ((a, b), cupsL')
 where
  step :: STRef s Int -> VUM.MVector s (Int, Int) -> ST s ()
  step currRef cups = do
    curr <- readSTRef currRef
    (currP, a) <- VUM.read cups curr
    (_, b) <- VUM.read cups a
    (_, c) <- VUM.read cups b
    (_, d) <- VUM.read cups c
    (_, dNext) <- VUM.read cups d
    VUM.write cups curr (currP, d)
    VUM.write cups d (curr, dNext)

    let hand = [a, b, c]
        dest = head $ filter (`notElem` hand) ([curr -1, curr -2 .. 1] <> [maxCup, maxCup -1 .. curr + 1])

    (destPrev, destNext) <- VUM.read cups dest
    (_, destNextNext) <- VUM.read cups destNext
    VUM.write cups dest (destPrev, a)
    VUM.write cups a (dest, b)
    VUM.write cups b (dest, c)
    VUM.write cups c (dest, destNext)
    VUM.write cups destNext (c, destNextNext)

    writeSTRef currRef d

  listFromIdx :: Int -> Int -> VUM.MVector s (Int, Int) -> [Int] -> ST s [Int]
  listFromIdx _ 0 _ acc = pure $ reverse acc
  listFromIdx idx n v acc = do
    (_, k) <- VUM.read v idx
    listFromIdx k (n -1) v (idx : acc)