module Day13 where

import Data.Either.Extra (mapLeft)
import Data.List (sortOn)
import Data.Maybe (catMaybes, mapMaybe)
import Data.Text (Text)
import qualified Data.Text as Text
import Data.Void (Void)
import Day (Day (Day))
import Text.Megaparsec
import Text.Megaparsec.Char
import Text.Megaparsec.Char.Lexer

day :: Day
day = Day challengeOne challengeTwo

challengeOne :: Text -> Text
challengeOne = Text.pack . show . fmap (uncurry (*) . findDepartBusAndDelay) . parseInput

challengeTwo :: Text -> Text
challengeTwo = Text.pack . show . fmap (findStart . snd) . parseInput

type Input = (Integer, [Maybe Integer])

parseInput :: Text -> Either String Input
parseInput = mapLeft show . runParser inputP "<show>"

findDepartBusAndDelay :: Input -> (Integer, Integer)
findDepartBusAndDelay (target, buses) =
  head $ sortOn snd ((\b -> (b, (b * ((target - 1) `div` b + 1)) - target)) <$> catMaybes buses)

findStart :: [Maybe Integer] -> Integer
findStart buses =
  let realBuses = mapMaybe (\case (i, Just b) -> Just (i, b); _ -> Nothing) $ [0 ..] `zip` buses
   in findNextStart 1 0 realBuses
 where
  findNextStart :: Integer -> Integer -> [(Integer, Integer)] -> Integer
  findNextStart _ n [] = n
  -- n' > n, n' = n + k*d (k >= 0), n'+i mod b == 0
  -- d' = lkkt d b
  findNextStart d n ((i, b) : bs) | (n + i) `mod` b == 0 = findNextStart (lcm d b) n bs
  findNextStart d n bs = findNextStart d (n + d) bs

type P = Parsec Void Text

inputP :: P Input
inputP = do
  target <- decimal
  _ <- newline
  buses <- choice [Just <$> decimal, Nothing <$ char 'x'] `sepBy` char ','
  return (target, buses)