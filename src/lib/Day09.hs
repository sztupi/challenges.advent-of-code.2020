module Day09 where

import Data.Sequence (Seq (..), (<|))
import qualified Data.Sequence as Seq
import Data.Text (Text)
import qualified Data.Text as Text
import qualified Data.Text.Read as Text.Read
import Day (Day (Day))

day :: Day
day = Day challengeOne challengeTwo

challengeOne :: Text -> Text
challengeOne = Text.pack . show . fmap (findFirstInvalid 25) . traverse (fmap fst . Text.Read.decimal) . Text.lines

challengeTwo :: Text -> Text
challengeTwo = Text.pack . show . fmap (findSetAddingUpToInvalid 25) . traverse (fmap fst . Text.Read.decimal) . Text.lines

findFirstInvalid :: Int -> [Integer] -> Maybe Integer
findFirstInvalid preamble nums = ffi (reverse $ take preamble nums) (drop preamble nums)
 where
  ffi :: [Integer] -> [Integer] -> Maybe Integer
  ffi _ [] = Nothing
  ffi pre (n : ns) =
    if n `elem` [x' + y' | x <- pre `zip` [1 ..], y <- pre `zip` [1 ..], snd x /= snd y, let x' = fst x, let y' = fst y]
      then ffi (n : take preamble pre) ns
      else Just n

findSetAddingUpToInvalid :: Int -> [Integer] -> Maybe Integer
findSetAddingUpToInvalid preamble nums = do
  target <- findFirstInvalid preamble nums
  range <- findRangeAddingUpTo nums target
  return $ minimum range + maximum range

findRangeAddingUpTo :: [Integer] -> Integer -> Maybe (Seq Integer)
findRangeAddingUpTo nums target =
  findRange Seq.empty nums
 where
  findRange :: Seq Integer -> [Integer] -> Maybe (Seq Integer)
  findRange _ [] = Nothing
  findRange sq _ | sum sq == target = Just sq
  findRange sq (n : ns) | sum sq < target = findRange (n <| sq) ns
  findRange (sq :|> _) ns = findRange sq ns
