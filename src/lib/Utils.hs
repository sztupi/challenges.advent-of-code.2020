module Utils where

import Data.String (IsString, fromString)

groupings :: forall a. (IsString a, Eq a) => [a] -> [[a]]
groupings = groupings' ([], [])
  where
    groupings' :: ([a], [[a]]) -> [a] -> [[a]]
    groupings' (g, gs) [] = reverse (reverse g : gs)
    groupings' (g, gs) (l : ls) | l == fromString "" = groupings' ([], reverse g : gs) ls
    groupings' (g, gs) (l : ls) = groupings' (l : g, gs) ls