{-# LANGUAGE FlexibleContexts #-}

module Day17 where

import Control.Monad ((<=<))
import Data.List (foldl')
import Data.Set (Set)
import qualified Data.Set as Set
import Data.Text (Text)
import qualified Data.Text as Text
import Day (Day (Day))

day :: Day
day = Day challengeOne challengeTwo

challengeOne :: Text -> Text
challengeOne = Text.pack . show . Set.size . steps 6 . parseInput @Coord3

challengeTwo :: Text -> Text
challengeTwo = Text.pack . show . Set.size . steps 6 . parseInput @Coord4

type Conway a = Set a

class (Eq a, Ord a) => ConwayCoord a where
  neighbours :: a -> [a]
  mkCoord :: (Int, Int) -> a

  activeNeighbourCount :: Conway a -> a -> Int
  activeNeighbourCount conway = length . filter (`Set.member` conway) . neighbours

steps :: ConwayCoord a => Int -> Conway a -> Conway a
steps n = foldl' (.) id (replicate n step)

step :: ConwayCoord a => Conway a -> Conway a
step active =
  let inactive = (Set.fromList . (neighbours <=< Set.toList)) active Set.\\ active
   in Set.union
        ( Set.fromList
            . filter ((\n -> n == 2 || n == 3) . activeNeighbourCount active)
            . Set.toList
            $ active
        )
        ( Set.fromList
            . filter ((== 3) . activeNeighbourCount active)
            . Set.toList
            $ inactive
        )

parseInput :: ConwayCoord a => Text -> Conway a
parseInput input =
  Set.fromList
    [ mkCoord (x, y)
    | (y, line) <- [0 ..] `zip` Text.lines input
    , (x, char) <- [0 ..] `zip` Text.unpack line
    , char == '#'
    ]

type Coord3 = (Int, Int, Int)

instance ConwayCoord Coord3 where
  neighbours (x, y, z) =
    [ (x + dx, y + dy, z + dz)
    | dx <- [-1 .. 1]
    , dy <- [-1 .. 1]
    , dz <- [-1 .. 1]
    , dx /= 0 || dy /= 0 || dz /= 0
    ]

  mkCoord (x, y) = (x, y, 0)

type Coord4 = (Int, Int, Int, Int)

instance ConwayCoord Coord4 where
  neighbours (x, y, z, w) =
    [ (x + dx, y + dy, z + dz, w + dw)
    | dx <- [-1 .. 1]
    , dy <- [-1 .. 1]
    , dz <- [-1 .. 1]
    , dw <- [-1 .. 1]
    , dx /= 0 || dy /= 0 || dz /= 0 || dw /= 0
    ]

  mkCoord (x, y) = (x, y, 0, 0)
