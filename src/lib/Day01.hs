module Day01
  ( day
  , findAddUpMult
  )
where

import           Day                            ( Day(Day) )
import           Data.Either
import           Data.Maybe
import           Data.Text                      ( Text )
import qualified Data.Text                     as Text
import qualified Data.Text.Read                as Text

day :: Day
day = Day challengeOne challengeTwo

challengeOne :: Text -> Text
challengeOne = Text.pack . show . findAddUpMult 2 2020 . expenses

challengeTwo :: Text -> Text
challengeTwo = Text.pack . show . findAddUpMult 3 2020 . expenses

expenses :: Text -> [Int]
expenses = fmap fst . rights . fmap Text.decimal . Text.lines

findAddUpMult :: Int -> Int -> [Int] -> Maybe Int
findAddUpMult n s xs = listToMaybe $ product <$> findAddUp n s xs

findAddUp :: Int -> Int -> [Int] -> [[Int]]
findAddUp 0 _ _                      = []
findAddUp n _ xs     | length xs < n = []
findAddUp n s (x:xs) | x > s         = findAddUp n s xs
findAddUp 1 s xs                     = (:[]) <$> filter (== s) xs
findAddUp n s (x:xs)                 = [ x:yss | yss <- findAddUp (n-1) (s-x) xs ] <> findAddUp n s xs
