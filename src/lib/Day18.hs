module Day18 where

import Control.Monad.Combinators.Expr
import Data.Either.Extra (mapLeft)
import Data.Text (Text)
import qualified Data.Text as Text
import Data.Void (Void)
import Day (Day (Day))
import Text.Megaparsec hiding (parse)
import Text.Megaparsec.Char
import qualified Text.Megaparsec.Char.Lexer as L

day :: Day
day = Day challengeOne challengeTwo

challengeOne :: Text -> Text
challengeOne = Text.pack . show . fmap sum . mapM (fmap eval . parse1) . Text.lines

challengeTwo :: Text -> Text
challengeTwo = Text.pack . show . fmap sum . mapM (fmap eval . parse2) . Text.lines

data Op
  = OAdd
  | OMult
  deriving (Show, Eq)

data Expr
  = EOp Op Expr Expr
  | ENumLit Int
  deriving (Show, Eq)

eval :: Expr -> Int
eval (ENumLit n) = n
eval (EOp OAdd l r) = eval l + eval r
eval (EOp OMult l r) = eval l * eval r

type Prec = [[Operator P Expr]]

prec1 :: Prec
prec1 =
  [
    [ InfixL (EOp OAdd <$ symbol "+")
    , InfixL (EOp OMult <$ symbol "*")
    ]
  ]

prec2 :: Prec
prec2 =
  [
    [ InfixL (EOp OAdd <$ symbol "+")
    ]
  ,
    [ InfixL (EOp OMult <$ symbol "*")
    ]
  ]

parse1 :: Text -> Either Text Expr
parse1 = parse prec1

parse2 :: Text -> Either Text Expr
parse2 = parse prec2

parse :: Prec -> Text -> Either Text Expr
parse prec = mapLeft (Text.pack . show) . runParser (exprLineP prec) "<none>"

type P = Parsec Void Text

lexeme :: P a -> P a
lexeme = L.lexeme space

symbol :: Text -> P Text
symbol = L.symbol space

exprLineP :: Prec -> P Expr
exprLineP prec = exprP prec <* eof

exprP :: Prec -> P Expr
exprP prec =
  makeExprParser
    (termP prec)
    prec

termP :: Prec -> P Expr
termP prec =
  choice
    [ ENumLit <$> lexeme L.decimal
    , between (symbol "(") (symbol ")") (exprP prec)
    ]
