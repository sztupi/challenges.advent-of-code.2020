module Day05 where

import Data.List (sort)
import Data.Text (Text)
import qualified Data.Text as Text
import Day (Day (Day))

day :: Day
day = Day challengeOne challengeTwo

challengeOne :: Text -> Text
challengeOne = Text.pack . show . maximum . fmap seatId . Text.lines

challengeTwo :: Text -> Text
challengeTwo = Text.pack . show . findGap . fmap seatId . Text.lines

seatId :: Text -> Int
seatId = seatId' 0 . Text.unpack
  where
    seatId' s [] = s
    seatId' s (h : t) | h `elem` ['B', 'R'] = seatId' (s * 2 + 1) t
    seatId' s (_ : t) = seatId' (s * 2) t

findGap :: [Int] -> Int
findGap = findGap' . sort
  where
    findGap' [] = error "should've found it..."
    findGap' (x1 : x2 : xs) | x2 == x1 + 1 = findGap (x2 : xs)
    findGap' (x : _) = x + 1