module Day25 where

import Data.Foldable
import Data.List (unfoldr)
import Data.Text (Text)
import qualified Data.Text as Text
import Data.Text.Read (decimal)
import Day (Day (Day))

day :: Day
day = Day challengeOne challengeTwo

challengeOne :: Text -> Text
challengeOne = Text.pack . show . findHandshake . parseInput

challengeTwo :: Text -> Text
challengeTwo = Text.pack . undefined

findHandshake :: (Int, Int) -> Int
findHandshake (pubCard, pubDoor) =
  f' (step 7) 1 0
 where
  f' :: (Int -> Int) -> Int -> Int -> Int
  f' _ v l | v == pubCard = runSteps pubDoor l
  f' _ v l | v == pubDoor = runSteps pubCard l
  f' s v l = f' s (s v) (l + 1)

runSteps :: Int -> Int -> Int
runSteps sn n = foldl' (.) id (replicate n (step sn)) 1

step :: Int -> Int -> Int
step sn v = (v * sn) `mod` 20201227

parseInput :: Text -> (Int, Int)
parseInput input =
  let (Right (a, _) : Right (b, _) : _) = decimal <$> Text.lines input
   in (a, b)