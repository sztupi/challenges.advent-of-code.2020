module Day20 where

import Control.Monad (guard, join)
import Data.Bifunctor
import Data.Bool (bool)
import Data.Either.Extra (mapLeft)
import qualified Data.List as List
import Data.Map (Map)
import qualified Data.Map.Strict as Map
import Data.Maybe (catMaybes)
import Data.Set (Set)
import qualified Data.Set as Set
import Data.Text (Text)
import qualified Data.Text as Text
import Data.Vector (Vector, (!), (!?))
import qualified Data.Vector as Vector
import Data.Void (Void)
import Day (Day (Day))
import Text.InterpolatedString.QM (qnb)
import Text.Megaparsec hiding (parse)
import Text.Megaparsec.Char hiding (space)
import qualified Text.Megaparsec.Char.Lexer as L

day :: Day
day = Day challengeOne challengeTwo

challengeOne :: Text -> Text
challengeOne = Text.pack . show . fmap (product . findCorners) . parseTiles

challengeTwo :: Text -> Text
challengeTwo = Text.pack . show . fmap findRoughSea . parseTiles

type TileId = Int
newtype Tile = Tile {runTile :: Vector (Vector Bool)}
  deriving (Eq, Ord)
type Tiles = Map TileId Tile

instance Show Tile where
  show (Tile t) =
    unlines $ Vector.toList . Vector.map (Vector.toList . Vector.map (bool '.' '#')) $ t

findCorners :: Tiles -> [TileId]
findCorners ts =
  [ tId
  | let tiles = Map.toList ts
  , (tId, tile) <- tiles
  , let matches = filter (not . null . (`List.intersect` edges tile) . edges) (snd <$> tiles)
  , length matches == 3
  ]
 where
  edges :: Tile -> [Vector Bool]
  edges (Tile t) =
    let e =
          [ t ! 0
          , Vector.map (! 9) t
          , t ! 9
          , Vector.map (! 0) t
          ]
     in join [e, Vector.reverse <$> e]

type Coord = (Int, Int)

data Edge = N | E | S | W

opEdge :: Edge -> Edge
opEdge N = S
opEdge E = W
opEdge S = N
opEdge W = E

edge :: Edge -> Tile -> Vector Bool
edge N = (! 0) . runTile
edge E = Vector.map (! 9) . runTile
edge S = (! 9) . runTile
edge W = Vector.map (! 0) . runTile

transpose :: Tile -> Tile
transpose = Tile . Vector.fromList . fmap Vector.fromList . List.transpose . fmap Vector.toList . Vector.toList . runTile

variations :: Tile -> [Tile]
variations (Tile t) =
  let (Tile t') = transpose (Tile t)
   in Tile
        <$> [ t
            , t'
            , Vector.reverse t
            , Vector.reverse t'
            , Vector.map Vector.reverse t
            , Vector.map Vector.reverse t'
            , Vector.reverse $ Vector.map Vector.reverse t
            , Vector.reverse $ Vector.map Vector.reverse t'
            ]

findGaps :: Set Coord -> Set Coord
findGaps coords =
  Set.fromList
    [ (x', y')
    | (x, y) <- Set.toList coords
    , (dx, dy) <- [(-1, 0), (1, 0), (0, -1), (0, 1)]
    , let (x', y') = (x + dx, y + dy)
    , (x', y') `Set.notMember` coords
    ]

type Image = Map Coord Tile

findFits :: Image -> Set Coord -> [Tile] -> [(Coord, Tile)]
findFits img coords vars =
  [(c, t) | c <- Set.toList coords, t <- vars, checkFit img c t]

checkFit :: Image -> Coord -> Tile -> Bool
checkFit img (x, y) tile =
  let neighs =
        catMaybes
          [ (N,) <$> Map.lookup (x, y -1) img
          , (E,) <$> Map.lookup (x + 1, y) img
          , (S,) <$> Map.lookup (x, y + 1) img
          , (W,) <$> Map.lookup (x -1, y) img
          ]
   in all (\(d, tile') -> edge d tile == edge (opEdge d) tile') neighs

buildImage :: Tiles -> Tile
buildImage (Map.elems -> (th : ts)) =
  let imageMap = createMap (Map.singleton (0, 0) th) (Set.fromList ts)
      coords = Map.keys imageMap
      minX = minimum $ fst <$> coords
      minY = minimum $ snd <$> coords
      maxX = maximum $ fst <$> coords
      maxY = maximum $ snd <$> coords
   in Tile $
        Vector.generate ((maxY - minY + 1) * 8) $ \y ->
          let (dy, my) = y `divMod` 8
           in Vector.generate ((maxX - minX + 1) * 8) $ \x ->
                let (dx, mx) = x `divMod` 8
                 in Just True == ((runTile <$> Map.lookup (minX + dx, minY + dy) imageMap) >>= (!? (my + 1)) >>= (!? (mx + 1)))
 where
  createMap :: Map Coord Tile -> Set Tile -> Map Coord Tile
  createMap image tiles =
    let images = do
          tile <- Set.toList tiles
          vtile <- variations tile
          gap <- Set.toList . findGaps $ Map.keysSet image
          guard $ checkFit image gap vtile
          return (gap, tile, vtile)
     in case images of
          [] -> image
          ((gap, tile, vtile) : _) -> createMap (Map.insert gap vtile image) (Set.delete tile tiles)

dragon :: Tile
dragon =
  parseTile
    [qnb|..................#.
         #....##....##....###
         .#..#..#..#..#..#...|]

dragonVs :: [Tile]
dragonVs = variations dragon

tileX, tileY :: Tile -> Int
tileX = Vector.length . (! 0) . runTile
tileY = Vector.length . runTile

tileToCoords :: Tile -> Set Coord
tileToCoords (Tile t) =
  Set.fromList $
    join . Vector.toList . flip Vector.imap t $ \y row ->
      join . Vector.toList . flip Vector.imap row $ \x ->
        \case True -> [(x, y)]; _ -> []

findRoughSea :: Tiles -> Int
findRoughSea (buildImage -> mapTile) =
  let (mx, my) = (tileX mapTile, tileY mapTile)
      mapCoords = tileToCoords mapTile
      dragonsCoords =
        Set.unions
          [ dCoords
          | drg <- dragonVs
          , let (drx, dry) = (tileX drg, tileY drg)
          , dx <- [0 .. mx - drx]
          , dy <- [0 .. my - dry]
          , let dCoords = Set.map (bimap (+ dx) (+ dy)) $ tileToCoords drg
          , dCoords `Set.isSubsetOf` mapCoords
          ]
   in Set.size $ mapCoords Set.\\ dragonsCoords

parseTile :: Text -> Tile
parseTile =
  Tile . Vector.fromList
    . fmap
      ( Vector.fromList
          . fmap (== '#')
          . Text.unpack
      )
    . Text.lines

parseTiles :: Text -> Either Text Tiles
parseTiles = mapLeft (Text.pack . show) . runParser tilesP "<none>"

type P = Parsec Void Text

tilesP :: P Tiles
tilesP = Map.fromList <$> tileP `sepBy` (newline >> newline)

tileP :: P (TileId, Tile)
tileP = do
  _ <- string "Tile "
  tileId <- L.decimal
  _ <- string ":" >> newline
  rows <- Vector.fromList <$> ((<>) <$> count 9 (rowP <* newline) <*> ((: []) <$> rowP))
  return (tileId, Tile rows)

rowP :: P (Vector Bool)
rowP = Vector.fromList <$> count 10 (choice [True <$ char '#', False <$ char '.'])
