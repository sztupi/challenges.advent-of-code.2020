module Day16 where

import Control.Monad (join)
import Data.Either.Extra (mapLeft)
import Data.List (transpose)
import Data.Set (Set)
import qualified Data.Set as Set
import Data.Text (Text)
import qualified Data.Text as Text
import Data.Void (Void)
import Day (Day (Day))
import Text.Megaparsec
    ( Parsec,
      (<|>),
      runParser,
      sepBy,
      someTill,
      MonadParsec(notFollowedBy, try) )
import Text.Megaparsec.Char
    ( string, char, newline, printChar, space )
import Text.Megaparsec.Char.Lexer ( decimal )

day :: Day
day = Day challengeOne challengeTwo

challengeOne :: Text -> Text
challengeOne = Text.pack . show . fmap scanningErrorRate . parseInput

challengeTwo :: Text -> Text
challengeTwo = Text.pack . show . fmap departureTicketValues . parseInput

type Ticket = [Int]

data Req = Req
  { rName :: Text
  , rRanges :: [(Int, Int)]
  }
  deriving (Show, Eq)

data Input = Input
  { iReqs :: [Req]
  , iMyTicket :: Ticket
  , iOtherTickets :: [Ticket]
  }
  deriving (Show, Eq)

scanningErrorRate :: Input -> Int
scanningErrorRate Input{..} =
  let tests = (\(mn, mx) x -> x >= mn && x <= mx) <$> (iReqs >>= rRanges)
   in sum $ filter (\x -> not $ any ($ x) tests) $ join iOtherTickets

dropInvalidTickets :: Input -> Input
dropInvalidTickets input@Input{..} =
  let tests = (\(mn, mx) x -> x >= mn && x <= mx) <$> (iReqs >>= rRanges)
   in input
        { iOtherTickets = filter (all (\x -> any ($ x) tests)) iOtherTickets
        }

labelTicket :: [Set Text] -> Ticket -> [(Text, Int)]
labelTicket labels ticket = (Set.elemAt 0 <$> labels) `zip` ticket

departureTicketValues :: Input -> Int
departureTicketValues i@Input{..} =
  product
    . fmap snd
    . filter (("departure" `Text.isPrefixOf`) . fst)
    $ labelTicket (inferLabels i) iMyTicket

inferLabels :: Input -> [Set Text]
inferLabels (dropInvalidTickets -> Input{..}) =
  let columns = transpose (iMyTicket : iOtherTickets)
      test req x = any (\(mn, mx) -> x >= mn && x <= mx) (rRanges req)
      potentialLabels =
        fmap
          ( \col ->
              Set.fromList $
                rName
                  <$> filter (\req -> all (test req) col) iReqs
          )
          columns
   in shrink potentialLabels

shrink :: [Set Text] -> [Set Text]
shrink ss =
  let singletons = Set.unions $ filter ((== 1) . Set.size) ss
      ss' =
        fmap
          ( \s ->
              if Set.size s < 2
                then s
                else s Set.\\ singletons
          )
          ss
   in if ss == ss' then ss' else shrink ss'

type P = Parsec Void Text

parseInput :: Text -> Either Text Input
parseInput = mapLeft (Text.pack . show) . runParser inputP "<none>"

inputP :: P Input
inputP = do
  iReqs <- reqP `sepBy` try (newline >> notFollowedBy newline)
  _ <- newline >> newline
  _ <- string "your ticket:" >> newline
  iMyTicket <- ticketP
  _ <- newline >> newline
  _ <- string "nearby tickets:" >> newline
  iOtherTickets <- ticketP `sepBy` newline
  return Input{..}

reqP :: P Req
reqP = do
  rName <- Text.pack <$> someTill (printChar <|> char ' ') (char ':')
  _ <- space
  rRanges <- rangeP `sepBy` string " or "
  return Req{..}

rangeP :: P (Int, Int)
rangeP = (,) <$> (decimal <* char '-') <*> decimal

ticketP :: P [Int]
ticketP = decimal `sepBy` char ','