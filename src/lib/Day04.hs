module Day04 where

import Data.Char (isDigit, isSpace)
import Data.Either.Extra (mapLeft)
import qualified Data.Map.Strict as Map
import Data.Text (Text)
import qualified Data.Text as Text
import Data.Void (Void)
import Day (Day (Day))
import Text.Megaparsec hiding (Pos)
import Text.Megaparsec.Char
import Text.Megaparsec.Char.Lexer (decimal)

-- import Text.Megaparsec.Char.Lexer

day :: Day
day = Day challengeOne challengeTwo

challengeOne :: Text -> Text
challengeOne = Text.pack . show . fmap (length . filter simpleValidPassport) . parsePassports

challengeTwo :: Text -> Text
challengeTwo = Text.pack . show . fmap (length . filter validPassport) . parsePassports

newtype Passport = Passport
  { ppFields :: [(Text, Text)]
  }
  deriving (Eq, Show)

parsePassports :: Text -> Either String [Passport]
parsePassports input = mapLeft show $ runParser fileParserP "<none>" input

type Rule = P Bool

runRule :: Rule -> Text -> Either String Bool
runRule rule = mapLeft show . runParser (rule <* eof) "<none>"

requiredFields :: [(Text, Rule)]
requiredFields =
  [ ( "byr",
      do
        num <- decimal
        return $ num >= 1920 && num <= 2002
    ),
    ( "iyr",
      do
        num <- decimal
        return $ num >= 2010 && num <= 2020
    ),
    ( "eyr",
      do
        num <- decimal
        return $ num >= 2020 && num <= 2030
    ),
    ( "hgt", do
      h <- decimal
      choice
        [ string "cm" >> do
            return $ h >= 150 && h <= 193,
          string "in" >> do
            return $ h >= 59 && h <= 76
        ]
    ),
    ("hcl", True <$ (char '#' >> count 6 (satisfy (`elem` ['0' .. '9'] <> ['a' .. 'f'])))),
    ("ecl", True <$ choice (string <$> ["amb", "blu", "brn", "gry", "grn", "hzl", "oth"])),
    ("pid", True <$ count 9 (satisfy isDigit))
  ]

simpleValidPassport :: Passport -> Bool
simpleValidPassport Passport {..} = all (`elem` (fst <$> ppFields)) (fst <$> requiredFields)

chkReqField :: (Text, Rule) -> Passport -> Bool
chkReqField (field, rule) Passport {ppFields = Map.fromList -> fieldMap} =
  case Map.lookup field fieldMap of
    Nothing -> False
    Just val -> runRule rule val == Right True

validPassport :: Passport -> Bool
validPassport passport =
  all (`chkReqField` passport) requiredFields

type P = Parsec Void Text

fileParserP :: P [Passport]
fileParserP =
  (passportP `sepBy` (eol *> eol)) <* eof

passportP :: P Passport
passportP =
  Passport <$> (fieldP `sepBy` try (satisfy isSpace *> notFollowedBy eol))

fieldP :: P (Text, Text)
fieldP = do
  key <- Text.pack <$> some (satisfy (\c -> c /= ':' && (not . isSpace) c))
  _ <- char ':'
  value <- Text.pack <$> some (satisfy (not . isSpace))
  return (key, value)
