module Day06 where

import qualified Data.Set as Set
import Data.Text (Text)
import qualified Data.Text as Text
import Day (Day (Day))
import Utils (groupings)

day :: Day
day = Day challengeOne challengeTwo

challengeOne :: Text -> Text
challengeOne = Text.pack . show . sum . countAnyoneQuestions . Text.lines

challengeTwo :: Text -> Text
challengeTwo = Text.pack . show . sum . countEveryoneQuestions . Text.lines

countAnyoneQuestions :: [Text] -> [Int]
countAnyoneQuestions lines =
  Set.size . Set.unions . fmap (Set.fromList . Text.unpack) <$> groupings lines

countEveryoneQuestions :: [Text] -> [Int]
countEveryoneQuestions lines =
  Set.size . foldl1 Set.intersection . fmap (Set.fromList . Text.unpack) <$> groupings lines