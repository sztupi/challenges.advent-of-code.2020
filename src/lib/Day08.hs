module Day08 where

import Control.Monad ((<=<))
import Data.Either.Extra (mapLeft)
import Data.Maybe (catMaybes)
import Data.Set (Set)
import qualified Data.Set as Set
import Data.Text (Text)
import qualified Data.Text as Text
import Data.Vector (Vector)
import qualified Data.Vector as Vector
import qualified Data.Vector.Mutable as MVector
import Data.Void (Void)
import Day (Day (Day))
import Text.Megaparsec
import Text.Megaparsec.Char
import Text.Megaparsec.Char.Lexer hiding (space)

day :: Day
day = Day challengeOne challengeTwo

challengeOne :: Text -> Text
challengeOne = Text.pack . show . (findRepeatInstructionAcc <=< parseProg)

challengeTwo :: Text -> Text
challengeTwo = Text.pack . show . (tryFixInstruction <=< parseProg)

data Instruction = Nop Int | Acc Int | Jmp Int

type Program = Vector Instruction

data ProgramState = ProgramState
  { psIP :: Int
  , psAcc :: Int
  }
  deriving (Show)

initialPS :: ProgramState
initialPS = ProgramState{psIP = 0, psAcc = 0}

data StepResult
  = Running ProgramState
  | Terminated ProgramState
  | RepeatInstruction ProgramState (Set Int)
  | ErrorResult ProgramState Text
  deriving (Show)

findRepeatInstructionAcc :: Program -> Either Text Int
findRepeatInstructionAcc prog =
  case runProgram prog of
    RepeatInstruction ProgramState{psAcc} _ -> Right psAcc
    sr -> Left ("unexpected termination" <> (Text.pack . show) sr)

tryFixInstruction :: Program -> Either Text Int
tryFixInstruction prog =
  case runProgram prog of
    RepeatInstruction _ instructions ->
      let candidates = catMaybes $ tryFix prog <$> Set.toList instructions
          runResults = runProgram <$> candidates
          terminated = catMaybes $ (\case Terminated st -> Just (psAcc st); _ -> Nothing) <$> runResults
       in case terminated of
            (acc : _) -> Right acc
            [] -> Left "no fixed program found"
    sr -> Left ("unexpected termination" <> (Text.pack . show) sr)

tryFix :: Program -> Int -> Maybe Program
tryFix prog instruction =
  case prog Vector.!? instruction of
    Nothing -> Nothing
    Just (Acc _) -> Nothing
    Just (Nop n) -> Just $ Vector.modify (\mv -> MVector.write mv instruction (Jmp n)) prog
    Just (Jmp n) -> Just $ Vector.modify (\mv -> MVector.write mv instruction (Nop n)) prog

runProgram :: Program -> StepResult
runProgram prog =
  runProgram' Set.empty initialPS
 where
  runProgram' :: Set Int -> ProgramState -> StepResult
  runProgram' past st =
    case step prog st of
      Running st' ->
        if psIP st' `Set.member` past
          then RepeatInstruction st past
          else runProgram' (psIP st' `Set.insert` past) st'
      sr -> sr

step :: Program -> ProgramState -> StepResult
step prog st@ProgramState{..} =
  if psIP == Vector.length prog
    then Terminated st
    else case prog Vector.!? psIP of
      Nothing -> ErrorResult st "invalid instruction pointer"
      Just (Nop _) -> Running st{psIP = psIP + 1}
      Just (Acc d) -> Running st{psIP = psIP + 1, psAcc = psAcc + d}
      Just (Jmp d) -> Running st{psIP = psIP + d}

type P = Parsec Void Text

parseProg :: Text -> Either Text Program
parseProg = mapLeft (Text.pack . show) . runParser progP "<none>"

progP :: P Program
progP = Vector.fromList <$> instructionP `sepBy` newline

instructionP :: P Instruction
instructionP =
  choice
    [ Nop <$> (string "nop " *> signed space decimal)
    , Acc <$> (string "acc " *> signed space decimal)
    , Jmp <$> (string "jmp " *> signed space decimal)
    ]
